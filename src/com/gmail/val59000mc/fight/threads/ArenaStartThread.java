package com.gmail.val59000mc.fight.threads;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.fight.Fight;
import com.gmail.val59000mc.fight.arena.Arena;
import com.gmail.val59000mc.fight.arena.Match;
import com.gmail.val59000mc.fight.configuration.Config;
import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.kits.Kit;
import com.gmail.val59000mc.fight.players.OTeam;
import com.gmail.val59000mc.fight.titles.TitleManager;
import com.gmail.val59000mc.spigotutils.Sounds;

public class ArenaStartThread implements Runnable {
	
	private Arena arena;
	private Match match;
	private Kit kit;
	private int remainingTime;
	
	private ArenaStartThread(Arena arena) {
		this.arena = arena;
		this.match = arena.getMatch();
		this.kit = arena.getMatch().getTeamA().getLeader().getKit();
		this.remainingTime = 5;
	}

	public void run() {
		
		Bukkit.getScheduler().runTask(Fight.getPlugin(), new Runnable(){

			public void run() {
				
				if(remainingTime == 0){
					match.startEndThread(arena);
					match.sendTeamsToArena(arena);
					return;
				}
				
				OTeam oTeamA = match.getTeamA();
				String teamASize = String.valueOf(oTeamA.getMembers().size());
						
				OTeam oTeamB = match.getTeamB();
				String teamBSize =  String.valueOf(oTeamB.getMembers().size());
				

				Set<Player> players;
				players = oTeamA.getPlayingPlayers();
				players.addAll(oTeamB.getPlayingPlayers());
				
				if(remainingTime == 5){
					oTeamA.sendMessage(I18n.get("fight.starting-in.subtitle")
					.replace("%opponent-size%", teamBSize)
					.replace("%team-size%",teamASize)
					.replace("%arena-group%", arena.getArenaGroup().getDisplayName())
					.replace("%kit%", kit.getDisplayName()));

					oTeamB.sendMessage(I18n.get("fight.starting-in.subtitle")
					.replace("%opponent-size%", teamASize)
					.replace("%team-size%",teamBSize)
					.replace("%arena-group%", arena.getArenaGroup().getDisplayName())
					.replace("%kit%", kit.getDisplayName()));
				}
				
				if(Config.isBountifulApiLoaded){

						TitleManager.sendAllTitle(
								oTeamA.getPlayingPlayers(), 
								I18n.get("fight.starting-in.title")
									.replace("%time%", String.valueOf(remainingTime)),
								I18n.get("fight.starting-in.subtitle")
									.replace("%opponent-size%", teamBSize)
									.replace("%team-size%",teamASize)
									.replace("%arena-group%", arena.getArenaGroup().getDisplayName())
									.replace("%kit%", kit.getDisplayName()),
								0,
								20,
								0
						);
						
						TitleManager.sendAllTitle(
								oTeamB.getPlayingPlayers(), 
								I18n.get("fight.starting-in.title")
									.replace("%time%", String.valueOf(remainingTime)),
								I18n.get("fight.starting-in.subtitle")
									.replace("%opponent-size%", teamASize)
									.replace("%team-size%",teamBSize)
									.replace("%arena-group%", arena.getArenaGroup().getDisplayName())
									.replace("%kit%", kit.getDisplayName()),
								0,
								20,
								10
						);
								
				}
				
				Sounds.playAll(players,Sound.NOTE_PLING,1,2);
				
				remainingTime--;
				
				Bukkit.getScheduler().runTaskLaterAsynchronously(Fight.getPlugin(), ArenaStartThread.this, 20);
				
			}
			
		});
		
	}

	public static void start(Arena arena) {
		Bukkit.getScheduler().runTaskLaterAsynchronously(Fight.getPlugin(), new ArenaStartThread(arena), 0);
	}

}
