package com.gmail.val59000mc.fight.threads;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.fight.Fight;
import com.gmail.val59000mc.fight.configuration.Config;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.PlayersManager;
import com.gmail.val59000mc.spigotutils.Logger;

public class PreventFarAwayPlayerThread implements Runnable {
	
	private static PreventFarAwayPlayerThread thread;
	private boolean run;
	
	public static void start(){
		Logger.debug("-> PreventFarAwayPlayerThread::start");
		PreventFarAwayPlayerThread thread = new PreventFarAwayPlayerThread();
		Bukkit.getScheduler().runTaskAsynchronously(Fight.getPlugin(), thread);
		Logger.debug("<- PreventFarAwayPlayerThread::start");
	}
	
	private PreventFarAwayPlayerThread() {
		thread = this;
		this.run = true;
	}
	
	public static void stop() {
		thread.run = false;
	}

	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(Fight.getPlugin(), new Runnable(){

			@Override
			public void run() {
				
				if(run){

					for(Player player : Bukkit.getOnlinePlayers()){

						OPlayer oPlayer = PlayersManager.instance().getOPlayer(player);
						
						if(oPlayer != null && player.getLocation().getY() < 0){
							Location location = null;
							
							if(oPlayer.isPlaying()){
								location = oPlayer.getTeam().getArena().getCenter();
							}else{
								location = Config.lobby;
							}
							
							player.teleport(location);
							player.setHealth(20);
						}
					}
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(Fight.getPlugin(), thread, 30);
				}
				
				
			}
			
		});
		
	}

}
