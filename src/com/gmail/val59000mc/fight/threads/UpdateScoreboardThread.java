package com.gmail.val59000mc.fight.threads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.gmail.val59000mc.fight.Fight;
import com.gmail.val59000mc.fight.arena.Match;
import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.OTeam;
import com.gmail.val59000mc.spigotutils.SimpleScoreboard;
import com.gmail.val59000mc.spigotutils.Time;

public class UpdateScoreboardThread implements Runnable{
	

	private static UpdateScoreboardThread instance;
	private Map<OPlayer,SimpleScoreboard> scoreboards;
	private int onlinePlayersCount;
	


	public static void start(){
		if(instance == null){
			instance = new UpdateScoreboardThread();
		}
		Bukkit.getScheduler().runTaskAsynchronously(Fight.getPlugin(), instance);
	}
	
	public static void add(OPlayer oPlayer){
		if(instance == null){
			instance = new UpdateScoreboardThread();
		}
		instance.addOPlayer(oPlayer);
	}
	
	private void addOPlayer(OPlayer oPlayer){
		if(!scoreboards.containsKey(oPlayer)){
			SimpleScoreboard sc = new SimpleScoreboard("--- §4Fight §r---");
			getScoreboards().put(oPlayer, sc);
		}
	}
	
	private UpdateScoreboardThread(){
		this.scoreboards = Collections.synchronizedMap(new HashMap<OPlayer,SimpleScoreboard>());
		this.onlinePlayersCount = 0;
	}
	
	private synchronized Map<OPlayer,SimpleScoreboard> getScoreboards(){
		return scoreboards;
	}
	
	public void run() {
		Bukkit.getScheduler().runTask(Fight.getPlugin(), new Runnable(){

			public void run() {
					
				onlinePlayersCount = Bukkit.getOnlinePlayers().size();
				
				synchronized (scoreboards) {
					for(Entry<OPlayer,SimpleScoreboard> entry : getScoreboards().entrySet()){
						OPlayer tPlayer = entry.getKey();
						SimpleScoreboard scoreboard = entry.getValue();
						if(tPlayer.isOnline()){
							updateScoreboard(tPlayer,scoreboard);
						}
					}
				}

				Bukkit.getScheduler().runTaskLaterAsynchronously(Fight.getPlugin(), instance, 20);
			}


			private void updateScoreboard(OPlayer oPlayer,	SimpleScoreboard scoreboard) {
				
				Player player = oPlayer.getPlayer();
				OTeam oTeam = oPlayer.getTeam();
				Match match = oTeam.getMatch();
				
				List<String> content = new ArrayList<String>();
				
				// RemainingTome
				if(match != null && oPlayer.isPlaying()){
					content.add(I18n.get("scoreboard.remaining-match-time",player));
					content.add(" §a"+Time.getFormattedTime(match.getRemainingTime()));
				}else{
					content.add(I18n.get("scoreboard.online-players",player));
					content.add(" "+ChatColor.GREEN+""+onlinePlayersCount);
				}
				
				
				// Online

				// ArenaGroup
				if(oTeam.getArenaGroup() != null){
					if(oPlayer.isWaiting()){
						content.add(I18n.get("scoreboard.fight-waiting",player));
					}else{
						content.add(I18n.get("scoreboard.fight",player));
					}
					content.add(" "+oTeam.getArenaGroup().getDisplayName());
				}
				
				// Team
				content.add(I18n.get("scoreboard.team",player));
				for(OPlayer oTeammate : oTeam.getMembers()){
					content.add(" "+ChatColor.GREEN+""+oTeammate.getName());
				}
				
				// Ennemies
				if(match != null){
					content.add(I18n.get("scoreboard.opponents",player));
					for(OPlayer oTeammate : match.getOpponent(oTeam).getMembers()){
						content.add(" "+ChatColor.RED+""+oTeammate.getName());
					}
				}
				
				scoreboard.clear();
				for(String line : content){
					scoreboard.add(line);
				}
				scoreboard.draw();
				scoreboard.send(player);
				
			}
				
			});
				
	}

	public static void setupTeamsColors(OPlayer oPlayer, OTeam team, OTeam opponent) {
		
		if(instance != null && instance.scoreboards.containsKey(oPlayer)){
			
			Scoreboard scoreboard = instance.scoreboards.get(oPlayer).getBukkitScoreboard();
			
			Team green = scoreboard.registerNewTeam("allies");
			Team red = scoreboard.registerNewTeam("enemies");
			green.setPrefix(ChatColor.GREEN+"");
			red.setPrefix(ChatColor.RED+"");
			
			// Putting players in colored teams
			for(OPlayer aOPlayer : team.getMembers()){
				green.addEntry(aOPlayer.getName());
			}
			

			for(OPlayer aOPlayer : opponent.getMembers()){
				red.addEntry(aOPlayer.getName());
			}	
			
		}
		
	}

	public static void removeTeamsColors(OPlayer oPlayer) {
		if(instance != null && instance.scoreboards.containsKey(oPlayer)){
			
			Scoreboard scoreboard = instance.scoreboards.get(oPlayer).getBukkitScoreboard();
			
			Team green = scoreboard.getTeam("allies");
			if(green != null){
				green.unregister();
			}
			
			Team red = scoreboard.getTeam("enemies");
			if(red != null){
				red.unregister();
			}
			
		}
		
	}
	
	
	
	
}
