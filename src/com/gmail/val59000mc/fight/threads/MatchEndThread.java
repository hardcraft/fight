package com.gmail.val59000mc.fight.threads;

import org.bukkit.Bukkit;

import com.gmail.val59000mc.fight.Fight;
import com.gmail.val59000mc.fight.arena.Arena;
import com.gmail.val59000mc.fight.arena.Match;
import com.gmail.val59000mc.fight.configuration.Config;

public class MatchEndThread implements Runnable {
	private int remainingTime;
	private Arena arena;
	private Match match;
	private boolean run;
	
	public MatchEndThread(Arena arena, Match match){
		this.remainingTime = Config.matchMaxTime;
		this.arena = arena;
		this.match = match;
		this.run = false;
	}

	public int getRemainingTime() {
		return remainingTime;
	}

	public void run() {
		
		Bukkit.getScheduler().runTask(Fight.getPlugin(), new Runnable(){

			public void run() {
				
				if(run){
					
					if(remainingTime == 0){
						arena.forceArenaEnd(match);
						return;
					}
					
					remainingTime--;
					
					if(remainingTime >= 0){
						Bukkit.getScheduler().runTaskLaterAsynchronously(Fight.getPlugin(), MatchEndThread.this, 20);
					}
					
				}
				
			}
			
		});
		
	}

	public void start(){
		this.run = true;
		Bukkit.getScheduler().runTaskLaterAsynchronously(Fight.getPlugin(), MatchEndThread.this, 0);
	}

	public void stop() {
		this.run = false;
	}

}
