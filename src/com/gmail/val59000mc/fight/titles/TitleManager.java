package com.gmail.val59000mc.fight.titles;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.connorlinfoot.bountifulapi.BountifulAPI;

public class TitleManager {
	
	public static void sendTitle(Player player, String message, int fadeIn, int stay, int fadeOut){
		BountifulAPI.sendTitle(player, fadeIn, stay, fadeOut, message, "");
	}
	
	public static void sendAllTitle(Set<Player> players, String message, String subtitle, int fadeIn, int stay, int fadeOut){
		for(Player player : players){
			BountifulAPI.sendTitle(player, fadeIn, stay, fadeOut, message, subtitle);
		}
	}
	
	public static void sendAllTitle(String message, int fadeIn, int stay, int fadeOut){
		for(Player player : Bukkit.getOnlinePlayers()){
			BountifulAPI.sendTitle(player, fadeIn, stay, fadeOut, message, "");
		}
	}

	public static void sendTitle(Player player, String message, String subtitle, int fadeIn, int stay, int fadeOut){
		BountifulAPI.sendTitle(player, fadeIn, stay, fadeOut, message, subtitle);
	}
}
