package com.gmail.val59000mc.fight.i18n;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;

public class English {

	public static Map<String, String> load() {
		Map<String,String> s = new HashMap<String,String>();

		// config
		s.put("config.dependency.vault-not-found", "Plugin Vault not found, no support for rewards");
		s.put("config.dependency.vault-loaded", "Plugin Vault loaded.");
		s.put("config.dependency.bountifulapi-not-found", "Plugin BountifulAPI not found, no support for titles");
		s.put("config.dependency.bountifulapi-loaded", "Plugin BountifulAPI loaded.");
		
		
		// map loader
		s.put("map-loader.load.no-last-world", "No last world to load, creating a new world");
		s.put("map-loader.load.last-world-not-found", "Last world not found, creating a new world.");
		s.put("map-loader.delete.no-last-world", "Not last world to delete.");
		s.put("map-loader.delete.last-world-not-found", "Last world to delete not found.");
		s.put("map-loader.delete", "Deleting last world.");
		s.put("map-loader.copy.not-found", "'winteriscoming' directory not found, cannot copy.");

		// game
		s.put("game.player-allowed-to-join", "Players are now allowed to join");
		
		// players
		s.put("player.not-allowed-to-join", "You are not allowed to join that game");
		s.put("player.welcome", "Welcome in Fight game !");
		s.put("player.full", "The game if full. If nobody logs out you will be a spectator !");
		s.put("player.spectate", "You are spectating.");
		s.put("player.red", ChatColor.RED+"Red team !");
		s.put("player.blue", ChatColor.BLUE+"Blue team !");
		s.put("player.green", ChatColor.GREEN+"Green team !");
		s.put("player.orange", ChatColor.GOLD+"Orange team !");
		s.put("player.coins-earned", ChatColor.GREEN+"HardCoins earned : ");
		s.put("player.ennemy-spawn-forbidden", ChatColor.RED+"You can't attack the ennemies' spawn");
		s.put("player.died", "%player% §rdied");
		s.put("player.killed", "%killer% &rkilled %killed%");
		s.put("player.wait-enderpearl", "§cPlease wait %time% before using another enderpearl.");
		s.put("player.kill-after-disconnect-warning", "§7%player% has disconnected. He has up to %time% to reconnect.");
		s.put("player.kill-after-disconnect", "§7%player% didn't log back in the game and has been eliminated.");
		s.put("player.reconnect", "§7%player% logged back in the game.");
		
		// Fight
		s.put("fight.target-not-online", "§c%player% is not online.");
		s.put("fight.syntax-error", "§cWrong syntax. Use §f/fight §cor §f/fight <player>");
		s.put("fight.not-available", "§cYou are already in a fight.");
		s.put("fight.not-waiting", "§cYou are not waiting for a fight.");
		s.put("fight.not-leader", "§cYou are not the leader of your team.");
		s.put("fight.target-not-leader", "§c%player% is not the leader of its team, %leader% is.");
		s.put("fight.already-waiting", "§cYou are already waiting for a fight to start.");
		s.put("fight.target-not-available", "§c%player% is already in a fight.");
		s.put("fight.opponent-chosing-map", "§aYour opponent will now choose an arena.");
		s.put("fight.request-sent-to", "§aFight request sent to %player%.");
		s.put("fight.request-already-sent", "§cFight request already sent to %player%");
		s.put("fight.request-received-from", "§aFight request §f[ §c%opponent-size% §fVS §a%team-size% §f] §afrom §c%player%");
		s.put("fight.accept-request", " §a[§fACCEPT§a]");
		s.put("fight.accept-request-hover", "§aAccept %player%'s fight request.");
		s.put("fight.choose-arena", "§cChoose the arena");
		s.put("fight.versus-player", "§fVersus %player%");
		s.put("fight.versus-team", "§fVersus %player%'s team");
		s.put("fight.self-team-request", "§cYou can't send a fight request to a player in your team.");
		s.put("fight.cancelled", "§6%player% has cancelled the fight.");
		s.put("fight.started", "§6The fight has started !");
		s.put("fight.starting-in.title", "§a%time%");
		s.put("fight.starting-in.subtitle", "§f[ §c%opponent-size% §fVS §a%team-size% §f] §f- %arena-group% §f- %kit%");

		// Team
		s.put("team.target-not-online", "§c%player% is not online.");
		s.put("team.syntax-error", "§cWrong syntax. Use §f/team <player>");
		s.put("team.not-available", "§cYou are in a fight and cannot modify your team.");
		s.put("team.target-not-available", "§c%player% is in a fight and cannot modify his team.");
		s.put("team.team-full", "§cYour team is full (3 players max).");
		s.put("team.target-team-full", "§c%player%'s team is full (3 players max).");
		s.put("team.target-already-in-team", "§c%player% is already in a team");
		s.put("team.not-leader", "§cYou are not the leader of your team.");
		s.put("team.request-sent-to", "§aTeam request sent to %player%.");
		s.put("team.request-already-sent", "§cTeam request already sent to %player%");
		s.put("team.request-received-from", "§aTeam request from %player%");
		s.put("team.accept-request", " §a[§fACCEPT§a]");
		s.put("team.accept-request-hover", "§aJoin %player%'s team.");
		s.put("team.self-team-request", "§cYou can't send a team request to a player in your team.");
		s.put("team.player-joined", "§a%player% has joined the team.");
		s.put("team.player-left", "§6%player% has left the team.");
		s.put("team.leave-solo-team", "§cYou are not in a team.");
		
		// Spec
		s.put("spec.now-spectating-player", "§aYou are spectating §f%player%§a. Use §f/spec leave §a to stop spectating.");
		s.put("spec.target-not-online", "§c%player% is not online.");
		s.put("spec.target-not-playing", "§c%player% is not fighting.");
		s.put("spec.syntax-error", "§cSyntax error, use §f/spec <player> §cor §f/spec leave§c.");
		s.put("spec.not-spectating", "§cYou are not spectating.");
		
		// Match
		s.put("match.not-leader", "§cYou are not the creator of the fight.");
		s.put("match.win.team", "%player%§r's team won the game !");
		s.put("match.win.player", "%player%§r won the game !");
		s.put("match.null", "§6Draw match !");
		s.put("match.cannot-change-arena-group", "§cYou can't change the arena now !");
		s.put("match.chosen-arena", "§aRegistered on the waiting queue for the §f%arena%§a arena.");
		s.put("match.not-registered", "§cYou are not registered in any arena waiting queue.");

		// Match
		s.put("kit.syntax-error", "§cUse the iron sword to select a kit.");
		s.put("kit.cannot-change", "§cYou can't change you kit now.");
		s.put("kit.not-found", "§cKit %kit% not found.");
		s.put("kit.selected", "§aKit %kit% selected.");
		
		
		// team
		s.put("team.RED", "Red");
		s.put("team.BLUE", "Blue");
		s.put("team.GREEN", "Green");
		s.put("team.ORANGE", "Orange");
				
		// scoreboard
		s.put("scoreboard.scores", "Scores");
		s.put("scoreboard.kills-deaths", "Kills/Deaths");
		s.put("scoreboard.team", "Team");
		s.put("scoreboard.opponents", "Opponents");
		s.put("scoreboard.fight-waiting", "Fight (waiting)");
		s.put("scoreboard.fight", "Fight");
		s.put("scoreboard.online-players", "Online");
		s.put("scoreboard.remaining-match-time", "Time");

		// ping
		s.put("ping.loading", "Loading");
		s.put("ping.playing", "Playing");
		s.put("ping.starting", "Starting");
		s.put("ping.waiting", "Waiting");
		s.put("ping.ended", "Ended");
				
				
		return s;
	}

}
