package com.gmail.val59000mc.fight.i18n;

import java.util.HashMap;
import java.util.Map;

public class French {

	public static Map<String, String> load() {
		Map<String,String> s = new HashMap<String,String>();
		
		// config
		s.put("config.dependency.vault-not-found", "Le plugin Vault est introuvable, pas de support pour les recompenses");
		s.put("config.dependency.vault-loaded", "Le plugin Vault a ete correctement charge.");
		s.put("config.dependency.bountifulapi-not-found", "Le plugin BountifulAPI est introuvable, pas de support pour les titres");
		s.put("config.dependency.bountifulapi-loaded", "Le plugin BountifulAPI a ete correctement charge.");
		
				
		// map loader
		s.put("map-loader.load.no-last-world", "Pas d'ancienne map a charger, creation d'une nouvelle map");
		s.put("map-loader.load.last-world-not-found", "Ancienne map non trouvee, creation d'une nouvelle map");
		s.put("map-loader.delete.no-last-world", "Pas d'ancienne map a supprimer.");
		s.put("map-loader.delete.last-world-not-found", "Ancienne map a supprimer non trouvee.");
		s.put("map-loader.delete", "Suppression de l'ancienne map.");
		s.put("map-loader.copy.not-found", "Dossier 'winteriscoming' introuvable, copie impossible.");
		
		// game
		s.put("game.player-allowed-to-join", "Les joueurs peuvent maintenant rejoindre la partie");

		// players
		s.put("player.not-allowed-to-join", "Vous n'etes pas autorise a rejoindre cette partie.");
		s.put("player.welcome", "Bienvenue dans le jeu Fight !");
		s.put("player.full", "La partie est pleine. Si personne ne se deconnecte, tu seras spectateur.");
		s.put("player.spectate", "Vous etes spectateur.");
		s.put("player.died", "%player% §rest mort");
		s.put("player.killed", "%killer% §ra tué %killed%");
		s.put("player.wait-enderpearl", "§cAttends %time% avant d'utiliser une autre perle.");
		s.put("player.kill-after-disconnect-warning", "§7%player% s'est déconnecté. Il a jusque %time% pour se reconnecter.");
		s.put("player.kill-after-disconnect", "§7%player% ne s'est pas reconnecté et a été éliminé.");
		s.put("player.reconnect", "§7%player% s'est reconnecté.");
		
		// Fight
		s.put("fight.target-not-online", "§c%player% n'est pas connecté.");
		s.put("fight.syntax-error", "§cMauvaise syntaxe. Tape §f/fight §cou §f/fight <joueur>");
		s.put("fight.not-available", "§cTu es déjà dans un fight.");
		s.put("fight.not-waiting", "§cTu n'es pas dans une file d'attente de fight.");
		s.put("fight.not-leader", "§cTu n'es pas le chef de ta team.");
		s.put("fight.target-not-leader", "§c%player% n'est pas le chef de sa team, %leader% l'est.");
		s.put("fight.already-waiting", "§cTu es déjà en train d'attendre qu'un fight commence.");
		s.put("fight.target-not-available", "§c%player% est déjà dans un fight.");
		s.put("fight.opponent-chosing-map", "§aTon adversaire va maintenant choisir l'arène.");
		s.put("fight.request-sent-to", "§aDemande de fight envoyée à %player%.");
		s.put("fight.request-already-sent", "§cDemande de fight déjà envoyée à %player%");
		s.put("fight.request-received-from", "§aDemande de fight §f[ §c%opponent-size% §fVS §a%team-size% §f] §ade §c%player%");
		s.put("fight.accept-request", " §a[§fACCEPTER§a]");
		s.put("fight.accept-request-hover","§aAccepter la demande de fight de %player%.");
		s.put("fight.choose-arena", "§cChoisis l'arène");
		s.put("fight.versus-player", "§fContre %player%");
		s.put("fight.versus-team", "§fContre la team de %player%");
		s.put("fight.self-team-request", "§cTu ne peux pas envoyer une demande de fight à un joueur de ta team.");
		s.put("fight.cancelled", "§6%player% a annulé le fight.");
		s.put("fight.started", "§6Le fight a commencé !");
		s.put("fight.starting-in.title", "§a%time%");
		s.put("fight.starting-in.subtitle", "§f[ §c%opponent-size% §fVS §a%team-size% §f] §f- %arena-group% §f- %kit%");

		// Team
		s.put("team.target-not-online", "§c%player% n'est pas connecté.");
		s.put("team.syntax-error", "§cMauvaise syntaxe. Tape §f/team <joueur>");
		s.put("team.not-available", "§cTu es dans un fight et ne peux pas modifier ta team.");
		s.put("team.not-leader", "§cTu n'es pas le chef de ta team.");
		s.put("team.target-not-available", "§c%player% est dans un fight et ne peut pas modifier sa team.");
		s.put("team.team-full", "§cTa team est pleine (3 joueurs max).");
		s.put("team.target-team-full", "§cLa team de %player% est pleine (3 joueurs max).");
		s.put("team.target-already-in-team", "§c%player% est déjà dans une team.");
		s.put("team.request-sent-to", "§aDemande de team envoyée à %player%.");
		s.put("team.request-already-sent", "§cDemande de team déjà envoyée à %player%");
		s.put("team.request-received-from", "§aDemande de team de %player%");
		s.put("team.accept-request", " §a[§fACCEPTER§a]");
		s.put("team.accept-request-hover", "§aRejoindre la team de %player%.");
		s.put("team.self-team-request", "§cTu ne peux pas envoyer une demande de team à un joueur de ta team.");
		s.put("team.player-joined", "§a%player% a rejoint la team.");
		s.put("team.player-left", "§6%player% a quitté la team.");
		s.put("team.leave-solo-team", "§cTu n'es dans aucune team.");
		
		// Spec
		s.put("spec.now-spectating-player", "§aTu es maintenant spectateur de §f%player%§a. Utilise §f/spec leave §apour ne plus être spectateur.");
		s.put("spec.target-not-online", "§c%player% n'est pas connecté.");
		s.put("spec.target-not-playing", "§c%player% n'est pas en train de combattre.");
		s.put("spec.syntax-error", "§cErreur de syntaxe, utilise §f/spec <joueur> §cou §f/spec leave§c.");
		s.put("spec.not-spectating", "§cTu n'es pas spectateur.");

		// Match
		s.put("match.not-leader", "§cTu n'es pas le créateur du fight.");
		s.put("match.win.team", "La team de %player%§r remporte la partie !");
		s.put("match.win.player", "%player%§r remporte la partie !");
		s.put("match.null", "§6Match nul !");
		s.put("match.cannot-change-arena-group", "§cTu ne peux pas changer d'arène maintenant !");
		s.put("match.chosen-arena", "§aInscrit dans la file d'attente pour l'arène §f%arena%§a.");
		s.put("match.not-registered", "§cTu n'es inscrit dans aucune file d'attente pour une arène.");

		// Match
		s.put("kit.syntax-error", "§cUtilise l'épée en fer pour choisir un kit.");
		s.put("kit.cannot-change", "§cTu ne peux pas changer de kit maintenant..");
		s.put("kit.not-found", "§cKit %kit% introuvable.");
		s.put("kit.selected", "§aKit %kit% sélectionné.");
		
		
		// team
		s.put("team.RED", "Rouge");
		s.put("team.BLUE", "Bleu");
		s.put("team.GREEN", "Vert");
		s.put("team.ORANGE", "Orange");
		
		// scoreboard
		s.put("scoreboard.scores", "Scores");
		s.put("scoreboard.kills-deaths", "Tués/Morts");
		s.put("scoreboard.team", "Team");
		s.put("scoreboard.opponents", "Ennemis");
		s.put("scoreboard.fight-waiting", "Fight (en attente)");
		s.put("scoreboard.fight", "Fight");
		s.put("scoreboard.online-players", "En ligne");
		s.put("scoreboard.remaining-match-time", "Temps");
		
		// ping
		s.put("ping.loading", "Chargement");
		s.put("ping.playing", "En jeu");
		s.put("ping.starting", "Commence");
		s.put("ping.waiting", "En attente");
		s.put("ping.ended", "Fin");
		
		return s;
	}

}
