package com.gmail.val59000mc.fight.configuration;

import java.util.List;
import java.util.regex.Matcher;

import net.milkbowl.vault.Vault;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.connorlinfoot.bountifulapi.BountifulAPI;
import com.gmail.val59000mc.fight.Fight;
import com.gmail.val59000mc.fight.actions.QueueArenaGroupActionParser;
import com.gmail.val59000mc.fight.actions.SelectKitActionParser;
import com.gmail.val59000mc.fight.arena.Arena;
import com.gmail.val59000mc.fight.arena.ArenaGroup;
import com.gmail.val59000mc.fight.arena.ArenaManager;
import com.gmail.val59000mc.fight.economy.VaultManager;
import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.kits.Kit;
import com.gmail.val59000mc.fight.kits.KitManager;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.api.SIGApi;
import com.gmail.val59000mc.simpleinventorygui.exceptions.InventoryParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ItemParseException;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.placeholders.RegexPlaceholder;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Parser;
import com.google.common.collect.Lists;

public class Config {
	
	// Dependencies
	public static boolean isVaultLoaded;
	public static boolean isBountifulApiLoaded;
	
	// World 
	public static World world;
	
	// Location
	public static Location lobby;
	public static LocationBounds lobbyBounds;
	
	// Match
	public static int matchMaxTime;
	
	// Rewards
	public static double winReward;
	public static double vipRewardBonus;
	public static double vipPlusRewardBonus;
	public static double helperRewardBonus;
	public static double builderRewardBonus;
	public static double moderateurRewardBonus;
	public static double administrateurRewardBonus;
	
	public static void load(){
		
		FileConfiguration cfg = Fight.getPlugin().getConfig();

		// Debug
		Logger.setDebug(cfg.getBoolean("debug",false));
		
		Logger.debug("-> Config::load");
		
		// World
		world = Bukkit.getWorlds().get(0);
		
		// Match
		matchMaxTime = cfg.getInt("match-max-time",300);
		
		// Locations
		lobby = Parser.parseLocation(world, cfg.getString("lobby.location"));
		lobbyBounds = new LocationBounds(Parser.parseLocation(world, cfg.getString("lobby.bounds.min")), Parser.parseLocation(world, cfg.getString("lobby.bounds.max")));
		
		// Rewards
		winReward = cfg.getDouble("reward.win",50);
		vipRewardBonus = cfg.getDouble("reward.vip",100);
		vipPlusRewardBonus = cfg.getDouble("reward.vip+",100);
		helperRewardBonus = cfg.getDouble("reward.helper",100);
		builderRewardBonus = cfg.getDouble("reward.builder",100);
		moderateurRewardBonus = cfg.getDouble("reward.moderateur",100);
		administrateurRewardBonus = cfg.getDouble("reward.administrateur",100);
		
		registerSigObjets();
		
		registerArenas();
		
		registerKits();
		
		
		
		// Dependencies
		loadVault();
		loadBountifulApi();
		VaultManager.setupEconomy();
		Logger.debug("<- Config::load");
	}
	
	private static void registerKits() {
		
		FileConfiguration cfg = Fight.getPlugin().getConfig();
		SIGApi sig = SIG.getPlugin().getAPI();
		KitManager km = KitManager.instance();
		km.reset();
		
		ConfigurationSection allKitsSection = cfg.getConfigurationSection("kits");
		if(allKitsSection != null){
			Logger.debug("Parsing kit section, size = "+allKitsSection.getKeys(false).size());
			
			for(String kitName : allKitsSection.getKeys(false)){ // for each kit
				Logger.debug("Parsing kit '"+kitName+"'");
				
				String displayName = allKitsSection.getString(kitName+".display-name");
				displayName = ChatColor.translateAlternateColorCodes('&', displayName);
				
				// Parsing items
				ConfigurationSection itemsSection = allKitsSection.getConfigurationSection(kitName+".items");
				List<Item> itemList = Lists.newArrayList();
				for(String itemName : itemsSection.getKeys(false)){
					try {
						itemList.add(sig.parseItemSection(itemsSection.getConfigurationSection(itemName)));
					} catch (ItemParseException e) {
						Logger.warnC(e.getMessage());
					}
				}

				// Parsing armor
				ConfigurationSection armorSection = allKitsSection.getConfigurationSection(kitName+".armor");
				List<Item> armorList = Lists.newArrayList();
				for(String armorName : armorSection.getKeys(false)){
					try {
						armorList.add(sig.parseItemSection(armorSection.getConfigurationSection(armorName)));
					} catch (ItemParseException e) {
						Logger.warnC(e.getMessage());
					}
				}

				km.addKit(new Kit(kitName, displayName,itemList,armorList));
			}
			
		}
	}

	private static void registerArenas() {
		
		FileConfiguration cfg = Fight.getPlugin().getConfig();
		
		ArenaManager am = ArenaManager.instance();
		am.reset();
		ConfigurationSection allArenaGroupsSection = cfg.getConfigurationSection("arena-groups");
		if(allArenaGroupsSection != null){
			Logger.debug("Parsing arena group section, size = "+allArenaGroupsSection.getKeys(false).size());
			
			for(String arenaGroupName : allArenaGroupsSection.getKeys(false)){ // for each arena group

				Logger.debug("Parsing arena-group '"+arenaGroupName+"'");
				ConfigurationSection arenaGroupSection = allArenaGroupsSection.getConfigurationSection(arenaGroupName);
				
				ArenaGroup arenaGroup = new ArenaGroup(arenaGroupName);
				arenaGroup.setDayTime(arenaGroupSection.getInt("time"));
				Logger.debug(" day-time = "+arenaGroup.getDayTime());
				
				arenaGroup.setDisplayName(ChatColor.translateAlternateColorCodes('&', arenaGroupSection.getString("display-name")));
				Logger.debug(" display-name = "+arenaGroup.getDisplayName());
				
				ConfigurationSection arenasSection = arenaGroupSection.getConfigurationSection("arenas");
				
				if(arenasSection != null){
					Logger.debug(" Parsing arenas section");
					
					for(String arenaName : arenasSection.getKeys(false)){ // for each arena in arena group
						ConfigurationSection arenaSection = arenasSection.getConfigurationSection(arenaName);
						Logger.debug("  Parsing arena '"+arenaName+"'");
						
						if(arenaSection != null){
							
							Location spawnA = Parser.parseLocation(world,arenaSection.getString("spawn-a"));
							Logger.debug("   spawn-a = "+Locations.printLocation(spawnA));
							
							
							Location spawnB = Parser.parseLocation(world,arenaSection.getString("spawn-b"));
							Logger.debug("   spawn-b = "+Locations.printLocation(spawnB));
							
							Location center = Parser.parseLocation(world,arenaSection.getString("center"));
							Logger.debug("   center = "+Locations.printLocation(center));
							
							Arena arena = new Arena(arenaGroup, spawnA, spawnB, center);
							arenaGroup.addArena(arena);
						}
					}
				}
				
				am.addArenaGroup(arenaGroup);
			}
		}
	}

	private static void registerSigObjets() {

		FileConfiguration cfg = Fight.getPlugin().getConfig();
		
		// SIG
		SIGApi sig = SIG.getPlugin().getAPI();
		sig.registerActionParser(new QueueArenaGroupActionParser());
		sig.registerActionParser(new SelectKitActionParser());
		
		ConfigurationSection inventories = cfg.getConfigurationSection("inventories");
		if(inventories != null){
			for(String invName : inventories.getKeys(false)){
				try {
					sig.registerInventory(sig.parseInventorySection(inventories.getConfigurationSection(invName)));
				} catch (InventoryParseException e) {
					Logger.severeC(e.getMessage());
				}
				
			}
		}

		sig.registerPlaceholder(new RegexPlaceholder("\\{fight\\.current-matches\\.\\w+\\}") {
			
			ArenaManager am = ArenaManager.instance();
			
			@Override
			public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
				Matcher matcher = getPattern().matcher(original);
			    if(matcher.find()) {
			    	String arenaGroupName = original.substring(matcher.start()+23, matcher.end()-1);
			    	ArenaGroup arenaGroup = am.getArenaGroup(arenaGroupName);
			    	original = matcher.replaceAll( ((arenaGroup == null ) ? "?" : String.valueOf(arenaGroup.getCurrentMatches()) ) );
			    }
			    
			    return original;
				
			}
		});
		
		sig.registerPlaceholder(new RegexPlaceholder("\\{fight\\.queue-length\\.\\w+\\}") {
			
			ArenaManager am = ArenaManager.instance();
			
			@Override
			public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
				Matcher matcher = getPattern().matcher(original);
			    if(matcher.find()) {
			    	String arenaGroupName = original.substring(matcher.start()+20, matcher.end()-1);
			    	ArenaGroup arenaGroup = am.getArenaGroup(arenaGroupName);
			    	original = matcher.replaceAll( ((arenaGroup == null ) ? "?" : String.valueOf(arenaGroup.getQueue().getLength()) ) );
			    }
			    
			    return original;
				
			}
		});
	}

	private static void loadVault(){
		Logger.debug("-> Config::loadVault");
		
		Plugin vault = Bukkit.getPluginManager().getPlugin("Vault");
        if(vault == null || !(vault instanceof Vault)) {
            Logger.warn(I18n.get("config.dependency.vault-not-found"));
        	 isVaultLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.vault-loaded"));
        	 isVaultLoaded = true;
        }
		Logger.debug("<- Config::loadVault");
	}
	
	private static void loadBountifulApi(){
		Logger.debug("-> Config::loadBountifulApi");
		
		Plugin bountifulApi = Bukkit.getPluginManager().getPlugin("BountifulAPI");
        if(bountifulApi == null || !(bountifulApi instanceof BountifulAPI)) {
            Logger.warn(I18n.get("config.dependency.bountiful-not-found"));
        	 isBountifulApiLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.bountifulapi-loaded"));
            isBountifulApiLoaded = true;
        }
		Logger.debug("<- Config::loadBountifulApi");
	}
		
		
}
