package com.gmail.val59000mc.fight.actions;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.fight.arena.ArenaGroup;
import com.gmail.val59000mc.fight.arena.ArenaManager;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.OTeam;
import com.gmail.val59000mc.fight.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class QueueArenaGroupAction extends Action{

	String arenaGroupName;
	
	public QueueArenaGroupAction(String arenaGroupName) {
		this.arenaGroupName = arenaGroupName;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		OPlayer oPlayer = PlayersManager.instance().getOPlayer(player);
		if(oPlayer != null){
			
			OTeam oTeam = oPlayer.getTeam();
			
			if(!oTeam.isTeamLeader(oPlayer)){
				oPlayer.sendI18nMessage("team.not-leader");
				oPlayer.playSound(Sound.VILLAGER_NO, 0.8f, 2f);
			}
			
			else if(!oPlayer.isWaiting() && !oPlayer.isAvailable()){
				oPlayer.sendI18nMessage("match.cannot-change-arena-group");
				oPlayer.playSound(Sound.VILLAGER_NO, 0.8f, 2f);
			}
			
			else if(oTeam.getMatch() != null && !oTeam.isMatchLeader(oPlayer)){
				oPlayer.sendI18nMessage("match.not-leader");
				oPlayer.playSound(Sound.VILLAGER_NO, 0.8f, 2f);
			}
			
			else{
				ArenaGroup arenaGroup = ArenaManager.instance().getArenaGroup(arenaGroupName);
				if(arenaGroup != null){
					ArenaManager.instance().addToQueue(arenaGroup,oPlayer);
				}
			}
			
		}
		executeNextAction(player, sigPlayer, true);
	}

}
