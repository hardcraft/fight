package com.gmail.val59000mc.fight.actions;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.ItemModifier;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class SelectKitAction extends Action implements ItemModifier{

	String kitName;
	private Item itemKitSelected;
	private Item itemKitNotSelected;
	
	public SelectKitAction(String kitName) {
		this.kitName = kitName;
		this.itemKitSelected = null;
		this.itemKitNotSelected = null;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		Bukkit.dispatchCommand(player, "kit "+kitName);
		executeNextAction(player, sigPlayer, true);
	}

	@Override
	public Item getAlternateVersion(Player player, SigPlayer sigPlayer) {
		OPlayer oPlayer = PlayersManager.instance().getOPlayer(player);
		if(oPlayer != null && oPlayer.getKit() != null){
			if(oPlayer.getKit().getName().equals(kitName)){
				return itemKitSelected;
			}else{
				return itemKitNotSelected;
			}
		}
		
		return itemKitNotSelected;
	}

	public void setAlternatesItems(Item itemKitSelected, Item itemKitNotSelected) {
		this.itemKitSelected = itemKitSelected;
		this.itemKitNotSelected = itemKitNotSelected;
	}

	@Override
	public boolean isModifier() {
		return itemKitSelected != null && itemKitNotSelected != null;
	}

}
