package com.gmail.val59000mc.fight.actions;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class QueueArenaGroupActionParser extends ActionParser{

	@Override
	public String getType() {
		return "queue-arena-group";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String arenaGroupName = action.getString("arena-group");
		if(arenaGroupName == null){
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a 'arena-group' attribute");		
		}
		return new QueueArenaGroupAction(arenaGroupName);
	}


}
