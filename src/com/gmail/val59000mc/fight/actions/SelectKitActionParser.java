package com.gmail.val59000mc.fight.actions;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ItemParseException;
import com.gmail.val59000mc.simpleinventorygui.items.Item.ItemBuilder;
import com.gmail.val59000mc.simpleinventorygui.items.ItemParser;

public class SelectKitActionParser extends ActionParser{

	@Override
	public String getType() {
		return "select-kit";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String kitName = action.getString("kit");
		if(kitName == null){
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must a kit attribute");		
		}
		
		SelectKitAction selectKitAction = new SelectKitAction(kitName);
		
		
		String itemKitSelected = action.getString("item-kit-selected");
		String itemKitNotSelected = action.getString("item-kit-not-selected");
		if(itemKitSelected != null && itemKitNotSelected != null){
			ItemBuilder builderSelected;
			ItemBuilder builderNotSelected;
			try {
				builderSelected = ItemParser.parseItemString(itemKitSelected);
			} catch (ItemParseException e) {
				throw new ActionParseException("#Error parsing item-kit-selected attribute of action '"+action.getName()+"' : "+e.getMessage());		
			}
			try {
				builderNotSelected = ItemParser.parseItemString(itemKitNotSelected);
			} catch (ItemParseException e) {
				throw new ActionParseException("#Error parsing item-kit-not-selected attribute of action '"+action.getName()+"' : "+e.getMessage());		
			}
			selectKitAction.setAlternatesItems(builderSelected.build(),builderNotSelected.build());
		}
		
		
		return selectKitAction;
	}

}
