package com.gmail.val59000mc.fight.commands;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.kits.Kit;
import com.gmail.val59000mc.fight.kits.KitManager;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.PlayersManager;

public class SelectKitCommandExecutor implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command,	String label, String[] args) {
		if(sender instanceof Player){
			
			PlayersManager pm = PlayersManager.instance();
			OPlayer oPlayer = pm.getOPlayer((Player) sender);
			
			switch(args.length){
				case 1:
					String kitName = args[0];
					changeKit(oPlayer, kitName);
					break;
				default:
					sender.sendMessage(I18n.get("kit.syntax-error"));
					break;
			}
			return true;
		}
		return false;
	}

	private void changeKit(OPlayer oPlayer, String kitName) {
		
		KitManager km = KitManager.instance();
		
		if(!oPlayer.isAvailable() && !oPlayer.isWaiting()){
			oPlayer.sendI18nMessage("kit.cannot-change");
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
		} else if(km.getKit(kitName) == null){
			oPlayer.sendMessage(I18n.get("kit.not-found").replace("%kit%",kitName));
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
		}else {
			Kit kit = km.getKit(kitName);
			oPlayer.setKit(kit);
			oPlayer.sendMessage(I18n.get("kit.selected").replace("%kit%",kit.getDisplayName()));
			oPlayer.playSound(Sound.PISTON_EXTEND, 1, 2f);
		}
		
	}

}