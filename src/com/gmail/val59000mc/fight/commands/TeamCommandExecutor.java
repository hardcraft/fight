package com.gmail.val59000mc.fight.commands;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.OTeam;
import com.gmail.val59000mc.fight.players.PlayersManager;

public class TeamCommandExecutor implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command,	String label, String[] args) {
		if(sender instanceof Player){
			
			PlayersManager pm = PlayersManager.instance();
			
			switch(args.length){
				case 1:
					String targetName = args[0];
					
					OPlayer oPlayer = pm.getOPlayer((Player) sender);
					OPlayer oTargetPlayer = pm.getOPlayer(targetName);
					
					if(targetName.equals("leave")){
						leaveTeam(oPlayer);
						return true;
					}
					
					if(oPlayer == null){
						return false;
					}
					
					if(oTargetPlayer == null){
						oPlayer.sendMessage(I18n.get("team.target-not-online").replace("%player%", targetName));
						return true;
					}
					
					askOrAcceptTeam(oPlayer, oTargetPlayer);
					break;
				default:
					sender.sendMessage(I18n.get("team.syntax-error"));
					break;
			}
			return true;
		}
		return false;
	}

	private void askOrAcceptTeam(OPlayer oPlayer, OPlayer oTargetPlayer) {

		OTeam oTeam = oPlayer.getTeam();
		OTeam oTargetTeam = oTargetPlayer.getTeam();
		
		if(!oPlayer.isAvailable()){
			oPlayer.sendI18nMessage("team.not-available");
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
			return;
		}
		
		if(!oTargetPlayer.isAvailable()){
			oPlayer.sendMessage(I18n.get("team.target-not-available").replace("%player%", oTargetPlayer.getName()));
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
			return;
		}
		
		if(oTeam.isTeamLeader(oPlayer)){
				
			if(oTeam.getTeamRequests().contains(oTargetPlayer.getTeam())){
				
				if(oTargetTeam.getMembers().size() == 3){
					oPlayer.sendMessage(I18n.get("team.target-team-full").replace("%player%",oTargetPlayer.getName()));
					oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
					return;
				}

				if(oTargetTeam.equals(oTeam)){
					oPlayer.sendMessage(I18n.get("team.self-team-request"));
					oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
					return;
				}
				
				oTeam.playSound(Sound.NOTE_PLING, 0.5f, 2f);
				oTeam.getTeamRequests().clear();
				
				oTargetTeam.playSound(Sound.NOTE_PLING, 0.5f, 2f);
				oTargetTeam.getTeamRequests().clear();
				
				oPlayer.joinTeam(oTargetTeam);
				oTargetTeam.sendMessage(I18n.get("team.player-joined").replace("%player%", oPlayer.getName()));
			}else{

				if(oTargetTeam.getMembers().size() > 1){
					oPlayer.sendMessage(I18n.get("team.target-already-in-team").replace("%player%", oTargetPlayer.getName()));
					oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
					return;
				}
				
				if(oTargetTeam.getTeamRequests().contains(oPlayer.getTeam())){
					oPlayer.sendMessage(I18n.get("team.request-already-sent").replace("%player%", oTargetPlayer.getName()));
					oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
					return;
				}
				
				if(oTeam.getMembers().size() == 3){
					oPlayer.sendMessage(I18n.get("team.team-full"));
					oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
					return;
				}

				if(oTargetTeam.equals(oTeam)){
					oPlayer.sendMessage(I18n.get("team.self-team-request"));
					oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
					return;
				}
				
				sendTeamRequest(oPlayer, oTargetPlayer);
			}
		}else{
			oPlayer.sendI18nMessage("team.not-leader");
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
		}	
	}

	private void sendTeamRequest(OPlayer oPlayer, OPlayer oTargetPlayer) {

		OTeam oTargetTeam = oTargetPlayer.getTeam();
		
		oPlayer.playSound(Sound.NOTE_PLING, 0.5f, 2f);
		oTargetPlayer.playSound(Sound.NOTE_PLING, 0.5f, 2f);
		oPlayer.sendMessage(I18n.get("team.request-sent-to").replace("%player%", oTargetPlayer.getName()));
		
        TextComponent message = new TextComponent(I18n.get("team.request-received-from").replace("%player%", oPlayer.getName()) );
        TextComponent accept = new TextComponent( I18n.get("team.accept-request") );
        accept.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/team "+ oPlayer.getName() ));
        accept.setHoverEvent( new HoverEvent(Action.SHOW_TEXT, new TextComponent[]{new TextComponent(I18n.get("team.accept-request-hover").replace("%player%", oPlayer.getName()))}));
        message.addExtra(accept);
        if(oTargetPlayer.isOnline()){
        	oTargetPlayer.getPlayer().spigot().sendMessage(message);
        }
        oTargetTeam.getTeamRequests().add(oPlayer.getTeam());
		
	}

	private void leaveTeam(OPlayer oPlayer) {
		OTeam oTeam = oPlayer.getTeam();
		
		if(!oPlayer.isAvailable()){
			oPlayer.sendI18nMessage("team.not-available");
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
			return;
		}

		if(oTeam.getMembers().size() == 1){
			oPlayer.sendI18nMessage("team.leave-solo-team");
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
			return;
		}

		oTeam.sendMessage(I18n.get("team.player-left").replace("%player%", oPlayer.getName()));
		oTeam.leave(oPlayer);
		oPlayer.playSound(Sound.NOTE_PLING, 0.5f, 2f);
	}

}
