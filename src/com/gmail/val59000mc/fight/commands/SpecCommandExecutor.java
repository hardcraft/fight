package com.gmail.val59000mc.fight.commands;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.fight.configuration.Config;
import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.PlayersManager;

public class SpecCommandExecutor implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command,	String label, String[] args) {
		if(sender instanceof Player){
			
			PlayersManager pm = PlayersManager.instance();
			
			switch(args.length){
				case 1:
					String targetName = args[0];
					
					OPlayer oPlayer = pm.getOPlayer((Player) sender);
					OPlayer oTargetPlayer = pm.getOPlayer(targetName);
					
					if(targetName.equals("leave")){
						unspecPlayer(oPlayer);
						return true;
					}
					
					if(oPlayer == null){
						return false;
					}
					
					if(oTargetPlayer == null){
						sender.sendMessage(I18n.get("spec.target-not-online").replace("%player%",args[0]));
						return true;
					}
					
					specPlayer(oPlayer,oTargetPlayer);
					break;
				default:
					sender.sendMessage(I18n.get("spec.syntax-error"));
					break;
			}
			return true;
		}
		return false;
	}

	private void unspecPlayer(OPlayer oPlayer) {
		if(!oPlayer.isSpectating()){
			oPlayer.sendMessage(I18n.get("spec.not-spectating"));
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
		}else if(oPlayer.isOnline()){
			PlayersManager.instance().availablePlayer(oPlayer);
			oPlayer.getPlayer().teleport(Config.lobby);
		}
		
	}

	private void specPlayer(OPlayer oPlayer, OPlayer oTargetPlayer) {
		if(oTargetPlayer.isPlaying()){
			PlayersManager.instance().spectatePlayer(oPlayer, oTargetPlayer);
		}else{
			oPlayer.sendMessage(I18n.get("spec.target-not-playing").replace("%player%",oTargetPlayer.getName()));
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
		}
		
	}

}
