package com.gmail.val59000mc.fight.commands;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.HoverEvent.Action;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.val59000mc.fight.Fight;
import com.gmail.val59000mc.fight.arena.ArenaManager;
import com.gmail.val59000mc.fight.arena.Match;
import com.gmail.val59000mc.fight.configuration.Config;
import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.OTeam;
import com.gmail.val59000mc.fight.players.PlayerState;
import com.gmail.val59000mc.fight.players.PlayersManager;
import com.gmail.val59000mc.fight.titles.TitleManager;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.spigotutils.Logger;

public class FightCommandExecutor implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command,	String label, String[] args) {
		if(sender instanceof Player){
			
			PlayersManager pm = PlayersManager.instance();
			OPlayer oPlayer = pm.getOPlayer((Player) sender);
			
			switch(args.length){
				case 0:
					ArenaManager.instance().openArenaGroupQueueInventory(oPlayer);
					break;
				case 1:
					String targetName = args[0];
					OPlayer oTargetPlayer = pm.getOPlayer(targetName);
					
					if(targetName.equals("reload")){
						if(sender.hasPermission("hardcraftpvp.fight.reload")){
							Logger.sendMessage((Player) sender, "§6Reloading Fight config.");
							reloadConfig();
						}
						return true;
					}
					
					if(targetName.equals("leave")){
						leaveFight(oPlayer);
						return true;
					}
					
					if(oPlayer == null){
						return false;
					}
					
					if(oTargetPlayer == null){
						sender.sendMessage(I18n.get("fight.target-not-online").replace("%player%", targetName));
						return true;
					}
					
					askOrAcceptFight(oPlayer, oTargetPlayer);
					break;
				default:
					sender.sendMessage(I18n.get("fight.syntax-error"));
					break;
			}
			return true;
		}
		return false;
	}

	private void reloadConfig() {
		SIG.getPlugin().reload();
		Config.load();
	}

	private static void leaveFight(OPlayer oPlayer) {
		
		OTeam oTeam = oPlayer.getTeam();

		if(!oTeam.isTeamLeader(oPlayer)){
			oPlayer.sendI18nMessage("fight.not-leader");
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
		}
		
		else if(!oPlayer.isWaiting() && !oPlayer.isAvailable()){
			oPlayer.sendI18nMessage("match.cannot-change-arena-group");
			oPlayer.playSound(Sound.VILLAGER_NO, 0.8f, 2f);
		}
		
		else if(!oPlayer.isWaiting()){
			oPlayer.sendI18nMessage("fight.not-waiting");
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
		}
		
		else{
			ArenaManager.instance().removeFromQueue(oPlayer);
		}

	}


	public static void askOrAcceptFight(OPlayer oPlayer, OPlayer oTargetPlayer) {
		
		OTeam oTeam = oPlayer.getTeam();
		OTeam oTargetTeam = oTargetPlayer.getTeam();

		if(!oPlayer.isAvailable()){
			oPlayer.sendI18nMessage("fight.not-available");
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
			return;
		}
		
		if(!oTargetPlayer.isAvailable()){
			oPlayer.sendMessage(I18n.get("fight.target-not-available").replace("%player%", oTargetPlayer.getName()));
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
			return;
		}
		
		if(oTeam.isTeamLeader(oPlayer)){
			if(!oTargetTeam.isTeamLeader(oTargetPlayer)){
				oPlayer.sendMessage(I18n.get("fight.target-not-leader")
						.replace("%player%", oTargetPlayer.getName())
						.replace("%leader%", oTargetPlayer.getTeam().getMembers().iterator().next().getName())
				);
				oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
				return;
			}
				
			if(oTeam.getFightRequests().contains(oTargetTeam)){
				
				if(oTargetTeam.equals(oTeam)){
					oPlayer.sendMessage(I18n.get("fight.self-team-request"));
					oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
					return;
				}
				
				oTeam.sendMessage(I18n.get("fight.opponent-chosing-map"));
				oTeam.playSound(Sound.ANVIL_USE, 0.5f, 2f);
				oTeam.getFightRequests().clear();
				
				oTargetTeam.playSound(Sound.ANVIL_USE, 0.5f, 2f);
				oTargetTeam.getFightRequests().clear();
				
				addMatch(oTargetTeam, oTeam);
				openArenaGroupQueueInventory(oTargetPlayer,oPlayer);
			}else{
				
				if(oTargetTeam.equals(oTeam)){
					oPlayer.sendMessage(I18n.get("fight.self-team-request"));
					oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
					return;
				}
				
				if(oTargetTeam.getFightRequests().contains(oTeam)){
					oPlayer.sendMessage(I18n.get("fight.request-already-sent").replace("%player%", oTargetPlayer.getName()));
					oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
					return;
				}
				
				sendFightRequest(oPlayer, oTargetPlayer);
			}
		}else{
			oPlayer.sendI18nMessage("fight.not-leader");
			oPlayer.playSound(Sound.VILLAGER_NO, 0.5f, 2f);
		}	
		
	}

	private static void addMatch(OTeam teamA, OTeam teamB) {
		Match match = new Match(teamA, teamB);
		
		teamA.setMatch(match);
		teamA.setState(PlayerState.WAITING);
		
		teamB.setMatch(match);
		teamB.setState(PlayerState.WAITING);
	}

	private static void sendFightRequest(OPlayer oPlayer, OPlayer oTargetPlayer) {
		
		oPlayer.playSound(Sound.ANVIL_USE, 0.5f, 2f);
		oTargetPlayer.playSound(Sound.ANVIL_USE, 0.5f, 2f);
		oPlayer.sendMessage(I18n.get("fight.request-sent-to").replace("%player%", oTargetPlayer.getName()));
		
		
		
        TextComponent message = new TextComponent(
        		I18n.get("fight.request-received-from")
        		.replace("%player%", oPlayer.getName())
        		.replace("%opponent-size%", String.valueOf(oPlayer.getTeam().getMembers().size()))
        		.replace("%team-size%", String.valueOf(oTargetPlayer.getTeam().getMembers().size()))
        );
        TextComponent accept = new TextComponent( I18n.get("fight.accept-request") );
        accept.setClickEvent( new ClickEvent( ClickEvent.Action.RUN_COMMAND, "/fight "+ oPlayer.getName() ));
        accept.setHoverEvent( new HoverEvent(Action.SHOW_TEXT, new TextComponent[]{new TextComponent(I18n.get("fight.accept-request-hover").replace("%player%", oPlayer.getName()))}));
        message.addExtra(accept);
        if(oTargetPlayer.isOnline()){
        	oTargetPlayer.getPlayer().spigot().sendMessage(message);
        }
        oTargetPlayer.getTeam().getFightRequests().add(oPlayer.getTeam());
	}
	
	private static void openArenaGroupQueueInventory(OPlayer oPlayer, OPlayer opponent){
		if(opponent == null){
			ArenaManager.instance().openArenaGroupQueueInventory(oPlayer);
			return;
		}
		
		if(Config.isBountifulApiLoaded && oPlayer.isOnline()){
			String versus;
			
			if(opponent.getTeam().getMembers().size() == 1)
				versus = I18n.get("fight.versus-player").replace("%player%", opponent.getName());
			else
				versus = I18n.get("fight.versus-team").replace("%player%", opponent.getName());
			
			TitleManager.sendTitle(oPlayer.getPlayer(), I18n.get("fight.choose-arena"), versus, 5, 30, 5);
		}
		
		new BukkitRunnable() {
			@Override
			public void run() {
				ArenaManager.instance().openArenaGroupQueueInventory(oPlayer);
			}
		}.runTaskLater(Fight.getPlugin(), 40);
	}

}
