package com.gmail.val59000mc.fight.kits;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.google.common.collect.Lists;

public class Kit {
	private String displayName;
	private String name;
	protected List<Item> items;
	protected List<Item> armor;
	
	public Kit(String name, String displayName, List<Item> items, List<Item> armor) {
		super();
		this.displayName = displayName;
		this.name = name;
		this.items = items;
		this.armor = armor;
	}
	

	public String getDisplayName() {
		return displayName;
	}

	public String getName() {
		return name;
	}

	public void giveKitToPlayer(Player player){
		SigPlayer sigPlayer = PlayersManager.getSigPlayer(player);
		if(sigPlayer != null){
			Inventories.setArmor(player, buildItemStackList(armor,player,sigPlayer));
			Inventories.give(player, buildItemStackList(items,player,sigPlayer));
		}
		
	}
	
	private List<ItemStack> buildItemStackList(List<Item> items, Player player, SigPlayer sigPlayer){
		List<ItemStack> itemStacks = Lists.newArrayList();
		for(Item item : items){
			itemStacks.add(item.buildRawItem(player, sigPlayer));
		}
		return itemStacks;
	}
	
	
}
