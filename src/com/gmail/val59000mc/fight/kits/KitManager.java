package com.gmail.val59000mc.fight.kits;

import java.util.Map;

import com.google.common.collect.Maps;

public class KitManager {
	
	
	private static KitManager instance;
	
	private Map<String,Kit> kits;

	public static KitManager instance(){
		if(instance == null){
			instance = new KitManager();
		}
		
		return instance;
	}

	private KitManager(){
		reset();
	}

	public void reset() {
		kits = Maps.newHashMap();
	}
	
	public void addKit(Kit kit){
		if(kits.containsKey(kit.getName())){
			throw new IllegalArgumentException("Kit "+kit.getName()+" is already registered.");
		}
		kits.put(kit.getName(), kit);
	}
	
	public Kit getKit(String name){
		return kits.get(name);
	}
}
