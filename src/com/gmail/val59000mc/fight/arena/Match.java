package com.gmail.val59000mc.fight.arena;

import java.util.List;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.fight.configuration.Config;
import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.kits.Kit;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.OTeam;
import com.gmail.val59000mc.fight.players.PlayerState;
import com.gmail.val59000mc.fight.players.PlayersManager;
import com.gmail.val59000mc.fight.threads.MatchEndThread;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;

public class Match {
	private OTeam teamA;
	private OTeam teamB;
	private MatchEndThread endThread;
	
	public Match(OTeam teamA, OTeam teamB) {
		this.teamA = teamA;
		this.teamB = teamB;
		this.endThread = null;
	}
	
	public int getRemainingTime(){
		if(endThread != null){
			return endThread.getRemainingTime();
		}
		return Config.matchMaxTime;
	}
	
	public void startEndThread(Arena arena){
		this.endThread = new MatchEndThread(arena,this);
		this.endThread.start();
	}
	
	public void stopEndThread(){
		this.endThread.stop();
	}

	public OTeam getTeamA() {
		return teamA;
	}

	public OTeam getTeamB() {
		return teamB;
	}
	
	public void winMatch(OTeam winner, OTeam loser){
		winner.rewardTeam(Config.winReward);

		String winMessage = winner.getWinMessage();
		winner.sendMessage("§a"+winMessage);
		loser.sendMessage("§c"+winMessage);
		
		winner.spectate();
		loser.spectate();
		
	}
	
	public void nullMatch(){

		teamA.sendMessage(I18n.get("match.null"));
		teamB.sendMessage(I18n.get("match.null"));
		
		teamA.spectate();
		teamB.spectate();
		
		teamA.playSound(Sound.ENDERDRAGON_GROWL, 1, 2);
		teamB.playSound(Sound.ENDERDRAGON_GROWL, 1, 2);
	}

	public void sendTeamsToLobby() {
		PlayersManager pm = PlayersManager.instance();
		pm.lobbyTeam(teamA);
		pm.lobbyTeam(teamB);
		teamA.playSound(Sound.ENDERDRAGON_GROWL, 1, 2);
		teamB.playSound(Sound.ENDERDRAGON_GROWL, 1, 2);
	}

	public void reset() {
		teamA.setArena(null);
		teamA.setArenaGroup(null);
		teamA.setState(PlayerState.AVAILABLE);
		teamA.setMatch(null);
		teamA = null;

		teamB.setArena(null);
		teamB.setArenaGroup(null);
		teamB.setState(PlayerState.AVAILABLE);
		teamB.setMatch(null);
		teamB = null;
	}

	public boolean contains(OTeam oTeam) {
		return oTeam.equals(teamA) || oTeam.equals(teamB);
	}

	public OTeam getOpponent(OTeam oTeam) {
		if(oTeam.equals(teamA))
			return teamB;
		else 
			return teamA;
					
	}

	public boolean isMatchLeader(OTeam oTeam) {
		return oTeam.equals(teamA);
	}

	public void sendTeamsToArena(Arena arena) {
		PlayersManager pm = PlayersManager.instance();
		Kit kit = teamA.getLeader().getKit();
		pm.arenaTeam(teamA,arena, arena.getSpawnA(), kit);
		pm.arenaTeam(teamB,arena, arena.getSpawnB(), kit);
	}

	
	public String toString(){
		return Objects.toStringHelper(this)
				.add("leaderOfTeamA", teamA.getLeader().getName())
				.add("leaderOfTeamB", teamB.getLeader().getName())
				.toString();
	}

	public List<Player> getOnlinePlayers() {
		List<Player> onlinePlayers = Lists.newArrayList();
		
		for(OPlayer oPlayer : teamA.getMembers()){
			if(oPlayer.isOnline()){
				onlinePlayers.add(oPlayer.getPlayer());
			}
		}
		
		for(OPlayer oPlayer : teamB.getMembers()){
			if(oPlayer.isOnline()){
				onlinePlayers.add(oPlayer.getPlayer());
			}
		}
		
		return onlinePlayers;
	}
	
	
}
