package com.gmail.val59000mc.fight.arena;

import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.gmail.val59000mc.fight.players.OTeam;
import com.gmail.val59000mc.fight.players.PlayerState;
import com.google.common.collect.Queues;

public class MatchQueue {
	private ConcurrentLinkedQueue<Match> queue;
	private ConcurrentLinkedQueue<OTeam> soloQueue;
	private Random r;
	
	public MatchQueue(){
		this.queue = Queues.newConcurrentLinkedQueue();
		this.soloQueue = Queues.newConcurrentLinkedQueue();
		this.r = new Random();
	}

	public void add(OTeam team) {
		soloQueue.add(team);
	}

	public void add(Match match) {
		queue.add(match);
	}

	public void removeMatch(OTeam oTeam) {
		Iterator<Match> it = queue.iterator();
		while(it.hasNext()){
			Match match = it.next();
			if(match.contains(oTeam)){
				it.remove();
			}
		}
	}

	public void removeTeam(OTeam oTeam) {
		soloQueue.remove(oTeam);
	}

	public int getLength() {
		return queue.size()+soloQueue.size();
	}

	public Match processQueue() {
		Match matchToStart = null;
		if(r.nextBoolean()){
			matchToStart = processMatchQueue();

			if(matchToStart == null){
				matchToStart = processSoloQueue();
			}
			
		}else{
			matchToStart = processSoloQueue();

			if(matchToStart == null){
				matchToStart = processMatchQueue();
			}
		}
		
		return matchToStart;
	}
	
	private Match processMatchQueue(){
		if(queue.size() >= 1){
			return queue.poll();
		}
		return null;
	}

	private Match processSoloQueue(){
		if(soloQueue.size() >= 2){
			OTeam teamA = soloQueue.poll();
			OTeam teamB = soloQueue.poll();

			Match match = new Match(teamA, teamB);
			
			teamA.setMatch(match);
			teamA.setState(PlayerState.WAITING);
			
			teamB.setMatch(match);
			teamB.setState(PlayerState.WAITING);
			
			return match;
		}
		
		return null;
	}
}
