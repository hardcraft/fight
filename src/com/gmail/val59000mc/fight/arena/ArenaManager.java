package com.gmail.val59000mc.fight.arena;

import java.util.Set;

import org.bukkit.Sound;

import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.OTeam;
import com.gmail.val59000mc.fight.players.PlayerState;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.spigotutils.Logger;
import com.google.common.collect.Sets;

public class ArenaManager {
	private static ArenaManager instance;
	
	private Set<ArenaGroup> arenaGroups;
	
	private ArenaManager(){
		reset();
	}

	public void reset() {
		arenaGroups = Sets.newConcurrentHashSet();
	}
	
	public static ArenaManager instance(){
		if(instance == null){
			instance = new ArenaManager();
		}
		return instance;
	}

	public void addArenaGroup(ArenaGroup arenaGroup){
		arenaGroups.add(arenaGroup);
	}

	protected void startDuel(Arena arena){
		arena.getMatch();
	}
	
	protected void resetArena(Arena arena){
		arena.setState(ArenaState.UNAVAILABLE);
		arena.setMatch(null);
		arena.clearGround();
	}

	public void checkArenaEnd(OPlayer oPlayer) {
		Logger.debug("-> ArenaManager::checkArenaEnd, oPlayer="+oPlayer);
		if(oPlayer.getTeam().getArena() != null){
			oPlayer.getTeam().getArena().checkArenaEnd();
		}
		Logger.debug("<- ArenaManager::checkArenaEnd");
	}

	public void checkArenaGroupEndWhenLeavingTeam(OPlayer tPlayer) {
		if(tPlayer.getTeam().getArenaGroup() != null){
			tPlayer.getTeam().getArenaGroup().checkArenaGroupEndWhenLeavingTeam(tPlayer);
		}
	}

	public void openArenaGroupQueueInventory(OPlayer oPlayer) {
		SIG.getPlugin().getAPI().openInventoryForPlayer(oPlayer.getPlayer(), "arena-group");
		
	}

	public void addToQueue(ArenaGroup arenaGroup, OPlayer oPlayer) {
		Logger.debug("-> ArenaManager::addToQueue, arenaGroup="+arenaGroup+", oPlayer="+oPlayer);

		String message = I18n.get("match.chosen-arena").replace("%arena%", arenaGroup.getDisplayName());
		OTeam oTeam = oPlayer.getTeam();
		
		if(oTeam.getMatch() != null){
			Match match = oTeam.getMatch();
			
			if(oTeam.getArenaGroup() != null){
				oTeam.getArenaGroup().removeFromQueue(oTeam,false);
			}
			
			match.getTeamA().sendMessage(message);
			match.getTeamB().sendMessage(message);
			match.getTeamA().playSound(Sound.ARROW_HIT, 0.5f, 1);
			match.getTeamB().playSound(Sound.ARROW_HIT, 0.5f, 1);
			arenaGroup.addToQueue(match);
		}else{
			
			if(oTeam.getArenaGroup() != null){
				oPlayer.getTeam().getArenaGroup().removeFromQueue(oTeam,false);
			}
			
			oTeam.sendMessage(message);
			oTeam.playSound(Sound.ARROW_HIT, 0.5f, 1);
			arenaGroup.addToQueue(oTeam);
		}
		
		arenaGroup.processQueue();

		Logger.debug("<- ArenaManager::addToQueue");
	}

	public void removeFromQueue(OPlayer oPlayer) {
		Logger.debug("-> ArenaManager::removeFromQueue, oPlayer="+oPlayer);
		
		OTeam oTeam = oPlayer.getTeam();

		if(oTeam.getArenaGroup() != null){
			oTeam.getArenaGroup().removeFromQueue(oTeam,true);
		}else if(oTeam.getMatch() != null){
			OTeam opponent = oTeam.getMatch().getOpponent(oTeam);
			
			opponent.setState(PlayerState.AVAILABLE);
			opponent.setArenaGroup(null);
			opponent.setArena(null);
			opponent.setMatch(null);

			oTeam.setState(PlayerState.AVAILABLE);
			oTeam.setArenaGroup(null);
			oTeam.setArena(null);
			oTeam.setMatch(null);
			
			String message = I18n.get("fight.cancelled").replace("%player%", oTeam.getLeader().getName());
			oTeam.sendMessage(message);
			oTeam.playSound(Sound.ARROW_HIT, 0.5f, 1);
			opponent.sendMessage(message);
			opponent.playSound(Sound.ARROW_HIT, 0.5f, 1);
			
		}else{
			oPlayer.sendI18nMessage("match.not-registered");
			oPlayer.playSound(Sound.VILLAGER_NO, 0.8f, 2f);
			return;
		}
		
		Logger.debug("<- ArenaManager::removeFromQueue");
	}

	public ArenaGroup getArenaGroup(String arenaGroupName) {
		for(ArenaGroup arenaGroup : arenaGroups){
			if(arenaGroup.getName().equals(arenaGroupName)){
				return arenaGroup;
			}
		}
		return null;
	}
}
