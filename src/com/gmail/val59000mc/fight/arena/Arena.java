package com.gmail.val59000mc.fight.arena;


import org.bukkit.Location;
import org.bukkit.entity.Entity;

import com.gmail.val59000mc.fight.players.OTeam;
import com.gmail.val59000mc.fight.players.PlayerState;
import com.gmail.val59000mc.fight.threads.ArenaStartThread;
import com.google.common.base.Objects;

public class Arena {
	private ArenaGroup arenaGroup; 
	private ArenaState state;
	
	private Match match;
	
	private Location spawnA;
	private Location spawnB;
	private Location center;
	
	public Arena(ArenaGroup arenaGroup, Location spawnA, Location spawnB, Location center) {
		this.arenaGroup = arenaGroup;
		this.spawnA = spawnA;
		this.spawnB = spawnB;
		this.center = center;
		this.match = null;
		this.state = ArenaState.AVAILABLE;
	}
	
	// Accessors
	
	public ArenaState getState() {
		return state;
	}
	public void setState(ArenaState state) {
		this.state = state;
	}
	public Match getMatch() {
		return match;
	}
	public void setMatch(Match match) {
		this.match = match;
	}
	public ArenaGroup getArenaGroup() {
		return arenaGroup;
	}
	public Location getSpawnA() {
		return spawnA;
	}
	public Location getSpawnB() {
		return spawnB;
	}
	public Location getCenter() {
		return center;
	}

	// Methods
	public boolean isAvailaible(){
		return this.state.equals(ArenaState.AVAILABLE);
	}

	public void startMatch(Match match){
		this.match = match;
		this.match.getTeamA().setArena(this);
		this.match.getTeamA().setState(PlayerState.PLAYING);
		
		this.match.getTeamB().setArena(this);
		this.match.getTeamB().setState(PlayerState.PLAYING);
		
		this.state = ArenaState.PLAYING;
		ArenaStartThread.start(this);
	}


	public void forceArenaEnd(Match match) {
		if(getState().equals(ArenaState.PLAYING) && getMatch() != null && match != null && match.equals(getMatch())){
			getMatch().nullMatch();
			getMatch().stopEndThread();
			setState(ArenaState.UNAVAILABLE);
			reset();
			getArenaGroup().processQueue();
		}
	}
	
	public void checkArenaEnd() {
		if(getState().equals(ArenaState.PLAYING)){

			OTeam a = getMatch().getTeamA();
			OTeam b = getMatch().getTeamB();
			int playingA = a.getPlayingMembers().size();
			int playingB = b.getPlayingMembers().size();
			
			if(playingA == 0 || playingB == 0){
				
				if(playingA == 0 && playingB == 0){
					getMatch().nullMatch();
				}else if(playingA == 0){
					getMatch().winMatch(b, a);
				}else if(playingB == 0){
					getMatch().winMatch(a, b);
				}
				getMatch().stopEndThread();
				setState(ArenaState.UNAVAILABLE);
				reset();
				getArenaGroup().processQueue();
			}
		}
		
	}

	protected void reset() {
		clearGround();
		getMatch().sendTeamsToLobby();
		getMatch().reset();
		match = null;
		setState(ArenaState.AVAILABLE);
	}

	public void clearGround() {
		for(Entity entity : getCenter().getWorld().getNearbyEntities(getCenter(), 40, 30, 40)){
			switch(entity.getType()){
				case ARMOR_STAND:
				case ENDER_CRYSTAL:
				case ITEM_FRAME:
				case PAINTING:
				case PLAYER:
				case VILLAGER:
					// don't remove
					break;
				default:
					entity.remove();
					break;
			}
		}
	}
	
	public String toString(){
		return Objects.toStringHelper(this)
				.add("state", state)
				.toString();
	}
	
}
