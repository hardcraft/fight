package com.gmail.val59000mc.fight.arena;

public enum ArenaState {
	AVAILABLE,
	UNAVAILABLE,
	PLAYING;
}
