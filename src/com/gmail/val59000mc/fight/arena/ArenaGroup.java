package com.gmail.val59000mc.fight.arena;

import java.util.Set;

import org.bukkit.Sound;

import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.OTeam;
import com.gmail.val59000mc.fight.players.PlayerState;
import com.google.common.base.Objects;
import com.google.common.collect.Sets;

public class ArenaGroup {
	private String name;
	private Set<Arena> arenas;
	private MatchQueue queue;
	private int dayTime;
	private String displayName;
	
	public ArenaGroup(String name) {
		super();
		this.name = name;
		this.displayName = name;
		this.arenas = Sets.newConcurrentHashSet();
		this.queue = new MatchQueue();
		this.dayTime = 0;
	}
	
	public void addArena(Arena arena){
		this.arenas.add(arena);
	}

	public int getDayTime() {
		return dayTime;
	}

	public String getName() {
		return name;
	}


	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public MatchQueue getQueue() {
		return queue;
	}

	public void setDayTime(int dayTime) {
		this.dayTime = dayTime;
	}

	public int getCurrentMatches() {
		int matches = 0;
		
		synchronized (arenas) {
			for(Arena arena : arenas){
				if(arena.getState().equals(ArenaState.PLAYING)){
					matches++;
				}
			}
		}
		
		return matches;
	}
	
	public void addToQueue(OTeam team){
		team.setArenaGroup(this);
		team.setState(PlayerState.WAITING);
		getQueue().add(team);
	}
	
	public void addToQueue(Match match){
		OTeam a = match.getTeamA();
		a.setState(PlayerState.WAITING);
		a.setArenaGroup(this);
		
		OTeam b = match.getTeamB();
		b.setState(PlayerState.WAITING);
		b.setArenaGroup(this);

		getQueue().add(match);
	}

	public void removeFromQueue(OTeam oTeam, boolean showMessage) {
		if(oTeam.getMatch() != null){
			
			
			OTeam opponent = oTeam.getMatch().getOpponent(oTeam);
			opponent.setState(PlayerState.AVAILABLE);
			opponent.setArenaGroup(null);
			opponent.setArena(null);
			oTeam.setState(PlayerState.AVAILABLE);
			oTeam.setArenaGroup(null);
			oTeam.setArena(null);
			
			if(showMessage){
				String message = I18n.get("fight.cancelled").replace("%player%", oTeam.getLeader().getName());
				oTeam.sendMessage(message);
				oTeam.playSound(Sound.ARROW_HIT, 0.5f, 1);
				opponent.sendMessage(message);
				opponent.playSound(Sound.ARROW_HIT, 0.5f, 1);
			}
			
			getQueue().removeMatch(oTeam);
		}else{
			oTeam.setState(PlayerState.AVAILABLE);
			oTeam.setArenaGroup(null);
			oTeam.setArena(null);
			
			if(showMessage){
				String message = I18n.get("fight.cancelled").replace("%player%", oTeam.getLeader().getName());
				oTeam.sendMessage(message);
				oTeam.playSound(Sound.ARROW_HIT, 0.5f, 1);
			}
			
			
			getQueue().removeTeam(oTeam);
		}
	}

	public void checkArenaGroupEndWhenLeavingTeam(OPlayer oPlayer) {
		if(oPlayer.getTeam().getMembers().size() == 1){
			removeFromQueue(oPlayer.getTeam(),false);
		}
	}

	public void processQueue() {
		Arena availableArena = null;
		for(Arena arena : arenas){
			if(arena.isAvailaible()){
				availableArena = arena;
				break;
			}
		}
		
		if(availableArena != null){
			Match match = getQueue().processQueue();
			if(match != null){
				availableArena.startMatch(match);
			}
		}
		
	}
	
	@Override
	public String toString(){
		return Objects.toStringHelper(this)
				.add("name", name)
				.add("displayName", displayName)
				.add("dayTime", dayTime)
				.toString();
	}
	
}
