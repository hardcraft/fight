package com.gmail.val59000mc.fight.maploader;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;

import com.gmail.val59000mc.fight.configuration.Config;
import com.gmail.val59000mc.spigotutils.Logger;


public class MapLoader {
	
	private static boolean deleteFile(File file) {

	    File[] flist = null;

	    if(file == null){
	        return false;
	    }

	    if (file.isFile()) {
	        return file.delete();
	    }

	    if (!file.isDirectory()) {
	        return false;
	    }

	    flist = file.listFiles();
	    if (flist != null && flist.length > 0) {
	        for (File f : flist) {
	            if (!deleteFile(f)) {
	                return false;
	            }
	        }
	    }

	    return file.delete();
	}

	
	public static void deleteOldPlayersFiles() {
		Logger.debug("-> MapLoader::deleteOldPlayersFiles");
		if(Bukkit.getServer().getWorlds().size()>0){
			// Deleting old players files
			File playersData = new File(Config.world.getName()+"/playerdata");
			deleteFile(playersData);
			playersData.mkdirs();
			
			// Deleting old players stats
			File playersStats = new File(Config.world.getName()+"/stats");
			deleteFile(playersStats);
			playersStats.mkdirs();
		}
		Logger.debug("<- MapLoader::deleteOldPlayersFiles");
	}
	
	private static void setupWorldGamerules(){
		Logger.debug("-> MapLoader::setupWorldGamerules");

		World world = Config.world;
		world.save();
		world.setGameRuleValue("doDaylightCycle", "false");
		world.setGameRuleValue("commandBlockOutput", "false");
		world.setGameRuleValue("logAdminCommands", "false");
		world.setGameRuleValue("sendCommandFeedback", "false");
		world.setGameRuleValue("doMobSpawning", "false");
		world.setGameRuleValue("doTileDrops", "false");
		world.setGameRuleValue("doFireTick", "false");
		world.setStorm(false);
		world.setWeatherDuration(999999999);
		world.setTime(18000);
		world.setDifficulty(Difficulty.HARD);
		world.setSpawnLocation(Config.lobby.getBlockX(), Config.lobby.getBlockY(), Config.lobby.getBlockZ());

		Logger.debug("<- MapLoader::setupWorldGamerules");
	}

	public static void load() {
		Logger.debug("-> MapLoader::load");
		setupWorldGamerules();
		Logger.debug("<- MapLoader::load");
	}
}
