package com.gmail.val59000mc.fight.game;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import com.gmail.val59000mc.fight.Fight;
import com.gmail.val59000mc.fight.commands.FightCommandExecutor;
import com.gmail.val59000mc.fight.commands.SelectKitCommandExecutor;
import com.gmail.val59000mc.fight.commands.SpecCommandExecutor;
import com.gmail.val59000mc.fight.commands.TeamCommandExecutor;
import com.gmail.val59000mc.fight.configuration.Config;
import com.gmail.val59000mc.fight.listeners.PingListener;
import com.gmail.val59000mc.fight.listeners.PlayerChatListener;
import com.gmail.val59000mc.fight.listeners.PlayerConnectionListener;
import com.gmail.val59000mc.fight.listeners.PlayerDamageListener;
import com.gmail.val59000mc.fight.listeners.PlayerDeathListener;
import com.gmail.val59000mc.fight.maploader.MapLoader;
import com.gmail.val59000mc.fight.threads.PreventFarAwayPlayerThread;
import com.gmail.val59000mc.fight.threads.UpdateScoreboardThread;
import com.gmail.val59000mc.spigotutils.Logger;


public class GameManager {

	private static GameManager instance = null;
	
	private GameState state;
	
	// static
	
	public static GameManager instance(){
		if(instance == null){
			instance = new GameManager();
		}
		return instance;
	}

	
	// constructor 
	
	private GameManager(){
		this.state = GameState.LOADING;
	}

	
	// accessors
	
	public GameState getState() {
		return state;
	}


	public boolean isState(GameState state) {
		return getState().equals(state);
	}
	
	// methods 
	
	public void loadGame() {
		state = GameState.LOADING;
		Logger.debug("-> GameManager::loadNewGame");
		
		Config.load();
		
		MapLoader.deleteOldPlayersFiles();
		MapLoader.load();
		
		registerListeners();
		registerCommands();
		
		playGame();
		
		Logger.debug("<- GameManager::loadNewGame");
	}

	
	private void registerListeners(){
		Logger.debug("-> GameManager::registerListeners");
		// Registers Listeners
		List<Listener> listeners = new ArrayList<Listener>();		
		listeners.add(new PlayerConnectionListener());	
		listeners.add(new PlayerChatListener());
		listeners.add(new PlayerDeathListener());
		listeners.add(new PingListener());
		listeners.add(new PlayerDamageListener());
		for(Listener listener : listeners){
			Logger.debug("Registering listener="+listener.getClass().getSimpleName());
			Bukkit.getServer().getPluginManager().registerEvents(listener, Fight.getPlugin());
		}
		Logger.debug("<- GameManager::registerListeners");
	}
	
	private void registerCommands(){
		Logger.debug("-> GameManager::registerCommands");
		// Registers Listeners
		Fight.getPlugin().getCommand("fight").setExecutor(new FightCommandExecutor());
		Fight.getPlugin().getCommand("spec").setExecutor(new SpecCommandExecutor());
		Fight.getPlugin().getCommand("team").setExecutor(new TeamCommandExecutor());
		Fight.getPlugin().getCommand("kit").setExecutor(new SelectKitCommandExecutor());
		Logger.debug("<- GameManager::registerCommands");
	}
	
	private void playGame(){
		state = GameState.PLAYING;
		UpdateScoreboardThread.start();
		PreventFarAwayPlayerThread.start();
	}

}
