package com.gmail.val59000mc.fight.players;

import java.util.List;
import java.util.Set;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.fight.arena.Arena;
import com.gmail.val59000mc.fight.arena.ArenaGroup;
import com.gmail.val59000mc.fight.arena.Match;
import com.gmail.val59000mc.fight.i18n.I18n;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;


public class OTeam {

	private List<OPlayer> members;
	private ArenaGroup arenaGroup;
	private Arena arena;
	private Set<OTeam> fightRequests;
	private Set<OTeam> teamRequests;
	private Match match;
	
	public OTeam(OPlayer playerA) {
		this.members = Lists.newArrayList(playerA);
		this.arena = null;
		this.arenaGroup = null;
		this.match = null;
		this.fightRequests = Sets.newHashSet();
		this.teamRequests = Sets.newHashSet();
	}
	public Arena getArena() {
		return arena;
	}
	public void setArena(Arena arena) {
		this.arena = arena;
	}
	public ArenaGroup getArenaGroup() {
		return arenaGroup;
	}
	public void setArenaGroup(ArenaGroup arenaGroup) {
		this.arenaGroup = arenaGroup;
	}
	public Match getMatch() {
		return match;
	}
	public void setMatch(Match match) {
		this.match = match;
	}
	public Set<OTeam> getFightRequests() {
		return fightRequests;
	}
	public Set<OTeam> getTeamRequests() {
		return teamRequests;
	}
	public void leave(OPlayer oPlayer) {
		getMembers().remove(oPlayer);
		oPlayer.setTeam(new OTeam(oPlayer));
	}
	public void join(OPlayer oPlayer) {
		getMembers().add(oPlayer);
		oPlayer.setTeam(this);
	}
	public boolean isTeamLeader(OPlayer tPlayer) {
		return (members.contains(tPlayer) && members.iterator().next().equals(tPlayer));
	}

	public boolean isMatchLeader(OPlayer oPlayer) {
		return(isTeamLeader(oPlayer) && getMatch() != null && getMatch().isMatchLeader(this));
	}

	public Set<OPlayer> getPlayingMembers() {
		
		synchronized (members) {
			Set<OPlayer> fightingMembers = Sets.newHashSet();
			
			for(OPlayer oPlayer : members){
				if(oPlayer.isPlaying()){
					fightingMembers.add(oPlayer);
				}
			}
			
			return fightingMembers;
		}
		
	}
	public Set<Player> getPlayingPlayers() {
		
		synchronized (members) {
			Set<Player> playingPlayers = Sets.newHashSet();
			
			for(OPlayer oPlayer : members){
				if(oPlayer.isPlaying()){
					playingPlayers.add(oPlayer.getPlayer());
				}
			}
			
			return playingPlayers;
		}
		
	}

	public List<OPlayer> getMembers() {
		return members;
	}

	public void playSound(Sound sound, float volume, float pitch) {
		synchronized (members) {

			for(OPlayer oPlayer : members){
				oPlayer.playSound(sound,volume,pitch);
			}
			
		}
	}

	public void setState(PlayerState state) {
		synchronized (members) {

			for(OPlayer oPlayer : members){
				oPlayer.setState(state);
			}
			
		}
	}

	public void rewardTeam(double winReward) {
		synchronized (members) {

			for(OPlayer oPlayer : members){
				oPlayer.rewardPlayer(winReward);
			}
			
		}
	}

	public void spectate() {
		
		synchronized (members) {

			for(OPlayer oPlayer : members){
				oPlayer.spectate();
			}
			
		}
		
	}

	public void sendMessage(String message) {
		synchronized (members) {

			for(OPlayer oPlayer : members){
				oPlayer.sendMessage(message);
			}
			
		}
	}

	public String getWinMessage() {
		if (members.size() == 1){
			return I18n.get("match.win.player").replace("%player%", getLeader().getName());
		}else{
			return I18n.get("match.win.team").replace("%player%", getLeader().getName());
		}
	}
	
	public String toString(){
		return Objects.toStringHelper(this)
				.add("members", members.size())
				.add("arenaGroup",arenaGroup)
				.add("arena",arena)
				.add("match",match)
				.toString();
	}
	public OPlayer getLeader() {
		return members.get(0);
	}
}
