package com.gmail.val59000mc.fight.players;


import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.fight.economy.VaultManager;
import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.kits.Kit;
import com.gmail.val59000mc.fight.kits.KitManager;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.google.common.base.Objects;

public class OPlayer {
	private String name;
	private OTeam team;
	private PlayerState state;
	private Kit kit;
	
	// Constructor
	
	public OPlayer(Player player){
		this.team = new OTeam(this);
		this.name = player.getName();
		this.state = PlayerState.AVAILABLE;
		this.kit = KitManager.instance().getKit("diamond-4");
	}
	
	
	
	// Accessors



	public String getName() {
		return name;
	}
	public Kit getKit() {
		return kit;
	}
	public void setKit(Kit kit) {
		this.kit = kit;
	}
	public PlayerState getState() {
		return state;
	}
	public void setState(PlayerState state) {
		this.state = state;
	}
	public OTeam getTeam() {
		return team;
	}
	public void setTeam(OTeam duel) {
		this.team = duel;
	}

	
	// Methods

	public Player getPlayer(){
		return Bukkit.getPlayer(name);
	}
	
	public Boolean isOnline(){
		return Bukkit.getPlayer(name) != null;
	}

	public void sendI18nMessage(String code) {
		if(isOnline()){
			Logger.sendMessage(getPlayer(), I18n.get(code,getPlayer()));
		}
	}

	public void sendMessage(String message) {
		if(isOnline()){
			Logger.sendMessage(getPlayer(), message);
		}
	}
	
	public String toString(){
		return Objects.toStringHelper(this)
				.add("name", name)
				.add("state",state)
				.add("team",team)
				.toString();
	}


	public boolean isState(PlayerState state) {
		return getState().equals(state);
	}
	
	public boolean isPlaying() {
		return isOnline() && isState(PlayerState.PLAYING);
	}
	
	public boolean isWaiting() {
		return isOnline() && isState(PlayerState.WAITING);
	}
	
	public boolean isSpectating() {
		return isOnline() && isState(PlayerState.SPECTATING);
	}
	
	public boolean isAvailable() {
		return isOnline() && isState(PlayerState.AVAILABLE);
	}

	public void leaveTeam(){
		getTeam().leave(this);
	}

	public void joinTeam(OTeam oTeam) {
		leaveTeam();
		oTeam.join(this);
	}


	public void playSound(Sound sound, float volume, float pitch) {
		if(isOnline())
			Sounds.play(getPlayer(), sound, volume, pitch);
	}



	public void rewardPlayer(double winReward) {
		if(isOnline()){
			double money = VaultManager.addMoney(getPlayer(), winReward);
			Sounds.play(getPlayer(), Sound.LEVEL_UP, 1, 2);
			getPlayer().sendMessage("§a+"+money+" §fHC");
		}
	}



	public void spectate() {
		setState(PlayerState.SPECTATING);
		if(isOnline())
			getPlayer().setGameMode(GameMode.SPECTATOR);
	}



	public boolean isInTeamWith(OPlayer tDamaged) {
		return getTeam().equals(tDamaged.getTeam());
	}

}
