package com.gmail.val59000mc.fight.players;

public enum PlayerState {
  AVAILABLE,
  SPECTATING,
  WAITING,
  PLAYING;
}
