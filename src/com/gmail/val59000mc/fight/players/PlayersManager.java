package com.gmail.val59000mc.fight.players;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.fight.Fight;
import com.gmail.val59000mc.fight.arena.Arena;
import com.gmail.val59000mc.fight.arena.ArenaManager;
import com.gmail.val59000mc.fight.configuration.Config;
import com.gmail.val59000mc.fight.i18n.I18n;
import com.gmail.val59000mc.fight.kits.Kit;
import com.gmail.val59000mc.fight.threads.UpdateScoreboardThread;
import com.gmail.val59000mc.fight.titles.TitleManager;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;

public class PlayersManager {
	private static PlayersManager instance;
	
	private List<OPlayer> players;
	
	// static
	
	public static PlayersManager instance(){
		if(instance == null){
			instance = new PlayersManager();
		}
		
		return instance;
	}
	
	// constructor 
	
	private PlayersManager(){
		players = Collections.synchronizedList(new ArrayList<OPlayer>());
	}
	
	
	// Accessors
	
	public OPlayer getOPlayer(Player player){
		return getOPlayer(player.getName());
	}
	
	public OPlayer getOPlayer(String name){
		for(OPlayer tPlayer : getPlayers()){
			if(tPlayer.getName().equals(name))
				return tPlayer;
		}
		
		return null;
	}
	
	public synchronized List<OPlayer> getPlayers(){
		return players;
	}
	
	
	// Methods 
	
	public synchronized OPlayer addPlayer(Player player){
		OPlayer newPlayer = new OPlayer(player);
		getPlayers().add(newPlayer);
		UpdateScoreboardThread.add(newPlayer);
		return newPlayer;
	}
	
	public synchronized void removePlayer(Player player){
		removePlayer(player.getName());
	}
	
	public synchronized void removePlayer(String name){
		OPlayer tPlayer = getOPlayer(name);
		if(tPlayer != null){
			tPlayer.spectate();
			ArenaManager.instance().checkArenaEnd(tPlayer);
			ArenaManager.instance().checkArenaGroupEndWhenLeavingTeam(tPlayer);
			tPlayer.leaveTeam();
			getPlayers().remove(tPlayer);
		}
	}
	
	public boolean isPlaying(Player player){
		OPlayer tPlayer = getOPlayer(player);
		if(tPlayer != null){
			return tPlayer.getState().equals(PlayerState.PLAYING);
		}
		return false;
	}

	public void playerJoinsTheGame(Player player) {
		Logger.debug("-> PlayersManager::playerJoinsTheGame, player="+player.getName());
		OPlayer oPlayer = getOPlayer(player);
		
		if(oPlayer == null){
			oPlayer = addPlayer(player);
		}
		
		availablePlayer(oPlayer);
		oPlayer.sendI18nMessage("player.welcome");
		Locations.safeTeleport(Fight.getPlugin(), player, Config.lobby);
		if(Config.isBountifulApiLoaded){
			TitleManager.sendTitle(player, "--- &4Fight &f---", 20, 20, 20);
		}
		
	}

	public void availablePlayer(OPlayer oPlayer){
		
		if(oPlayer.isOnline()){

			Player player = oPlayer.getPlayer();
			
			// Show all players
			for(Player otherPlayer : Bukkit.getOnlinePlayers()){
				player.showPlayer(otherPlayer);
			}
			
			Effects.clear(player);
			Inventories.clear(player);
			
			Effects.addPermanent(player, PotionEffectType.SATURATION, 0);
			Effects.addPermanent(player, PotionEffectType.SPEED, 1);
			player.setGameMode(GameMode.ADVENTURE);
			player.setHealth(20);
			player.setPlayerTime(18000, false);
			SIG.getPlugin().getAPI().giveInventoryToPlayer(player, "join");
			
			oPlayer.setState(PlayerState.AVAILABLE);
			
			resetPlayersVisibility(oPlayer);
		}
		
	}
	
	public void spectatePlayer(OPlayer oSpectator, OPlayer oFighter){

		if(oFighter.isPlaying() && oSpectator.isAvailable()){
			
			Player spectator = oSpectator.getPlayer();
			
			oSpectator.sendMessage(I18n.get("spec.now-spectating-player").replace("%player%",oFighter.getName()));
			
			// Clear spec inv and effects and teleport
			oSpectator.setState(PlayerState.SPECTATING);
			spectator.setGameMode(GameMode.SPECTATOR);
			Inventories.clear(spectator);
			Effects.clear(spectator);
			spectator.teleport(oFighter.getPlayer().getLocation());
			
			if(oFighter.getTeam() != null && oFighter.getTeam().getMatch() != null){
				setupMatchPlayersVisibility(oSpectator, oFighter.getTeam(), oFighter.getTeam().getMatch().getOpponent(oFighter.getTeam()));
			}
		}
		
	}
	
	public List<OPlayer> getPlayingPlayers() {
		List<OPlayer> playingPlayers = new ArrayList<OPlayer>();
		for(OPlayer p : getPlayers()){
			if(p.isPlaying()){
				playingPlayers.add(p);
			}
		}
		return playingPlayers;
	}
	
	public List<OPlayer> getWaitingPlayers() {
		List<OPlayer> waitingPlayers = new ArrayList<OPlayer>();
		for(OPlayer p : getPlayers()){
			if(p.isWaiting()){
				waitingPlayers.add(p);
			}
		}
		return waitingPlayers;
	}
	
	public List<OPlayer> getAvailablePlayers() {
		List<OPlayer> availablePlayers = new ArrayList<OPlayer>();
		for(OPlayer p : getPlayers()){
			if(p.isAvailable()){
				availablePlayers.add(p);
			}
		}
		return availablePlayers;
	}
	
	public List<OPlayer> getSpectatingPlayers() {
		List<OPlayer> spectatingPlayers = new ArrayList<OPlayer>();
		for(OPlayer p : getPlayers()){
			if(p.isAvailable()){
				spectatingPlayers.add(p);
			}
		}
		return spectatingPlayers;
	}

	public void registerDuelQueue(OTeam duel) {
		// TODO Auto-generated method stub
		
	}

	public void chooseDuelArena(OTeam duel) {
		// TODO Auto-generated method stub
		
	}

	public void lobbyTeam(OTeam team) {
		synchronized(team.getMembers()){
			for(OPlayer oPlayer : team.getMembers()){
				availablePlayer(oPlayer);
				if(oPlayer.isOnline()){
					Locations.safeTeleport(Fight.getPlugin(), oPlayer.getPlayer(), Config.lobby);
				}
			}
		}
	}

	public void arenaTeam(OTeam team, Arena arena, Location location, Kit kit) {
		

		
		synchronized(team.getMembers()){
			for(OPlayer oPlayer : team.getMembers()){
				if(oPlayer.isOnline()){
					
					Player player = oPlayer.getPlayer();
					
					Locations.safeTeleport(Fight.getPlugin(), player, location);
					
					setupMatchPlayersVisibility(oPlayer, team, arena.getMatch().getOpponent(team));
					
				}
				
				startPlayer(arena,oPlayer, kit);
			}
		}
	}

	private void resetPlayersVisibility(OPlayer oPlayer) {
		if(oPlayer.isOnline()){
			Player player = oPlayer.getPlayer();
			
			for(Player p : Bukkit.getOnlinePlayers()){
				player.showPlayer(p);
			}
			
			UpdateScoreboardThread.removeTeamsColors(oPlayer);
			
		}
	}
	
	private void setupMatchPlayersVisibility(OPlayer oPlayer, OTeam team, OTeam opponent) {

		if(oPlayer.isOnline()){

			Player player = oPlayer.getPlayer();
			
			for(Player p : Bukkit.getOnlinePlayers()){
				player.hidePlayer(p);
			}
			
			for(OPlayer p : team.getMembers()){
				if(p.isOnline()){
					player.showPlayer(p.getPlayer());
				}
			}
			
			for(OPlayer p : opponent.getMembers()){
				if(p.isOnline()){
					player.showPlayer(p.getPlayer());
				}
			}
			
			UpdateScoreboardThread.removeTeamsColors(oPlayer);
			UpdateScoreboardThread.setupTeamsColors(oPlayer, team, opponent);
		}
		
	}

	private void startPlayer(Arena arena, OPlayer oPlayer, Kit kit) {
		if(oPlayer.isOnline()){
			Player player = oPlayer.getPlayer();
			
			Inventories.clear(player);
			Effects.clear(player);
			Sounds.play(player, Sound.ENDERDRAGON_GROWL, 1, 2);
			kit.giveKitToPlayer(player);
			if(Config.isBountifulApiLoaded){
				TitleManager.sendTitle(player, "§cFIGHT !", 10, 20, 10);
				oPlayer.sendI18nMessage("fight.started");
			}
			player.setHealth(20);
			player.setFoodLevel(20);
			player.setPlayerTime(arena.getArenaGroup().getDayTime(), false);
			oPlayer.setState(PlayerState.PLAYING);
		}
	}

	public void displayKilledBy(OPlayer dead, OPlayer oKiller) {
		String toTeam = I18n.get("player.killed")
				.replace("%killed%", "§a"+dead.getName())
				.replace("%killer%", "§c"+oKiller.getName());
		String toOtherTeam = I18n.get("player.killed")
				.replace("%killed%", "§c"+dead.getName())
				.replace("%killer%", "§a"+oKiller.getName());
		dead.getTeam().sendMessage(toTeam);
		oKiller.getTeam().sendMessage(toOtherTeam);
	}

	public void displayKilled(OPlayer dead) {
		
		OTeam oTeam = dead.getTeam();
		
		String message = I18n.get("player.died").replace("%player%", "§a"+dead.getName());
		
		dead.getTeam().sendMessage(message);
		
		if(oTeam.getMatch() != null){
			oTeam.getMatch().getOpponent(oTeam).sendMessage(message);
		}
		
	}
	
}
