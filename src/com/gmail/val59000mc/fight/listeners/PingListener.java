package com.gmail.val59000mc.fight.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import com.gmail.val59000mc.fight.game.GameManager;
import com.gmail.val59000mc.fight.i18n.I18n;

public class PingListener implements Listener{
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPing(ServerListPingEvent event){
		GameManager gm = GameManager.instance();
		switch(gm.getState()){
			case LOADING:
				event.setMotd(I18n.get("ping.loading"));
				break;
			case PLAYING:
				event.setMotd(I18n.get("ping.playing"));
				break;
			default:
				event.setMotd(I18n.get("ping.loading"));
				break;
		
		}
	}
}
