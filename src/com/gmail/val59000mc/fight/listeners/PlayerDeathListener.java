package com.gmail.val59000mc.fight.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.gmail.val59000mc.fight.Fight;
import com.gmail.val59000mc.fight.arena.ArenaManager;
import com.gmail.val59000mc.fight.configuration.Config;
import com.gmail.val59000mc.fight.game.GameManager;
import com.gmail.val59000mc.fight.game.GameState;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.PlayerState;
import com.gmail.val59000mc.fight.players.PlayersManager;
import com.gmail.val59000mc.spigotutils.Logger;

public class PlayerDeathListener implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeath(final PlayerDeathEvent event) {

		OPlayer wicPlayer = PlayersManager.instance().getOPlayer(event.getEntity());
		
		if(wicPlayer == null || !wicPlayer.isOnline()){
			return;
		}
		
		handleDeathPlayer(event,wicPlayer);
		
	}
	
	public void handleDeathPlayer(final PlayerDeathEvent event, OPlayer dead){
		PlayersManager pm = PlayersManager.instance();
		
		// Add death score to dead player and kill score to killer if playing
		if(GameManager.instance().isState(GameState.PLAYING)){
			
			OPlayer oKiller = null;
			Player killer = event.getEntity().getKiller();
			if(killer != null){
				oKiller = pm.getOPlayer(killer);
			}
			
			if(oKiller != null && oKiller.isOnline() && dead.getTeam().getMembers().size() > 1){
				oKiller.rewardPlayer(Config.winReward);
				pm.displayKilledBy(dead,oKiller);
			}else{
				pm.displayKilled(dead);
			}
			
			event.getDrops().clear();
			event.setDroppedExp(0);
			event.setDeathMessage("");
			dead.spectate();
			ArenaManager.instance().checkArenaEnd(dead);
			
			Bukkit.getScheduler().runTaskLater(Fight.getPlugin(), new Runnable() {
				
				public void run() {
			        event.getEntity().spigot().respawn();
				}
			}, 20);

		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Logger.debug("-> PlayerDeathListener::onPlayerRespawn, player="+event.getPlayer().getName());
		
		handleRespawnPlayer(event);
		
		Logger.debug("<- PlayerDeathListener::onPlayerRespawn");
	}
	
	public void handleRespawnPlayer(PlayerRespawnEvent event){
		Logger.debug("-> PlayerDeathListener::handleRespawnPlayer, player="+event.getPlayer().getName());

		final String name = event.getPlayer().getName();
		OPlayer oPlayer = PlayersManager.instance().getOPlayer(name);
		
		
		if(oPlayer != null){
			Logger.debug("Respawning oPlayer="+oPlayer);
			
			if(oPlayer.isState(PlayerState.SPECTATING) && oPlayer.getTeam().getArena() != null){
				event.setRespawnLocation(oPlayer.getTeam().getArena().getCenter());
			}else{
				event.setRespawnLocation(Config.lobby);
				
				Bukkit.getScheduler().runTaskLater(Fight.getPlugin(), new Runnable() {
					
					public void run() {
						OPlayer p = PlayersManager.instance().getOPlayer(name);
						if(p != null){
							PlayersManager.instance().availablePlayer(p);
						}
					}
				}, 1);
			}
			
		}

	}
	
}
