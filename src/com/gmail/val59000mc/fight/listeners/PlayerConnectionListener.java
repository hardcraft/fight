package com.gmail.val59000mc.fight.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.gmail.val59000mc.fight.Fight;
import com.gmail.val59000mc.fight.players.PlayersManager;

public class PlayerConnectionListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoin(final PlayerJoinEvent event){
		Bukkit.getScheduler().runTaskLater(Fight.getPlugin(), new Runnable(){

			public void run() {
				PlayersManager.instance().playerJoinsTheGame(event.getPlayer());
			}
			
		}, 1);
		
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDisconnect(PlayerQuitEvent event){
		
		Player player = event.getPlayer();
		PlayersManager.instance().removePlayer(player);
	}
	
}
