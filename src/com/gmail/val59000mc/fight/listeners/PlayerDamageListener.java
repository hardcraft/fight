package com.gmail.val59000mc.fight.listeners;

import java.util.Collection;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.fight.commands.FightCommandExecutor;
import com.gmail.val59000mc.fight.configuration.Config;
import com.gmail.val59000mc.fight.players.OPlayer;
import com.gmail.val59000mc.fight.players.PlayerState;
import com.gmail.val59000mc.fight.players.PlayersManager;
import com.gmail.val59000mc.spigotutils.Logger;

public class PlayerDamageListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageByEntityEvent event){
		handleAskFightAtLobby(event);
		handleFriendlyFire(event);
		handleArrow(event);
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent event){
		handleAnyDamage(event);
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPotionSplash(PotionSplashEvent event){
		handlePotionSplash(event);
	}
	
	///////////////////////
	// PotionSplashEvent //
	///////////////////////
	
	private void handlePotionSplash(PotionSplashEvent event) {
		if(event.getEntity().getShooter() instanceof Player){
			PlayersManager pm = PlayersManager.instance();
			
			Player damager = (Player) event.getEntity().getShooter();
			OPlayer oDamager = pm.getOPlayer(damager);
			
			if(oDamager != null){
			
				if(isAttackPotion(event.getPotion())){
					// Cancelling potion damage for teamates
					for(LivingEntity living : event.getAffectedEntities()){
						if(living instanceof Player){
							Player damaged = (Player) living;
							OPlayer oDamaged = pm.getOPlayer(damaged);
							if(oDamager.isInTeamWith(oDamaged)){
								event.setIntensity(living, 0);
							}
						}
					}
				}
				
			}
		}
	
	}
	
	// Only checking the first potion effect, considering vanilla potions
	private boolean isAttackPotion(ThrownPotion potion){
		Collection<PotionEffect> effects = potion.getEffects();
		if(effects.size() > 0){
			PotionEffectType effect = effects.iterator().next().getType();
			return ( 
				effect.equals(PotionEffectType.HARM) ||
			    effect.equals(PotionEffectType.POISON) ||
			    effect.equals(PotionEffectType.WEAKNESS) ||
				effect.equals(PotionEffectType.SLOW) ||
			    effect.equals(PotionEffectType.SLOW_DIGGING) ||
			    effect.equals(PotionEffectType.CONFUSION) ||
			    effect.equals(PotionEffectType.BLINDNESS) ||
			    effect.equals(PotionEffectType.HUNGER) ||
			    effect.equals(PotionEffectType.WITHER)
			  );
		}
		return false;
	}


	///////////////////////
	// EntityDamageEvent //
	///////////////////////
	
	private void handleAnyDamage(EntityDamageEvent event){
		if(event.getEntity() instanceof Player){
			if(Config.lobbyBounds.contains(event.getEntity().getLocation())){
				event.setCancelled(true);
			}
		}
	}
	
	///////////////////////////////
	// EntityDamageByEntityEvent //
	///////////////////////////////


	private void handleAskFightAtLobby(EntityDamageByEntityEvent event) {

		if(event.getDamager() instanceof Player && event.getEntity() instanceof Player){
			Player damager = (Player) event.getDamager();
			Player damaged = (Player) event.getEntity();
			Logger.debug("-> handleAskFightAtLobby, damager="+damager.getName()+" , damaged)"+damaged.getName());
			
			boolean insideLobbyBounds = Config.lobbyBounds.contains(damaged.getLocation()) && Config.lobby.distanceSquared(damaged.getLocation()) > 5;
			Logger.debug("insideLobbyBounds="+insideLobbyBounds);
			
			if(insideLobbyBounds){
				PlayersManager pm = PlayersManager.instance();
				
				OPlayer oDamager = pm.getOPlayer(damager);
				OPlayer oDamaged = pm.getOPlayer(damaged);
				
				if(oDamager != null && oDamaged != null){
					FightCommandExecutor.askOrAcceptFight(oDamager, oDamaged);
				}
				
			}
			Logger.debug("<- handleAskFightAtLobby");
		}
	}
	
	private void handleFriendlyFire(EntityDamageByEntityEvent event){

		PlayersManager pm = PlayersManager.instance();
		
		if(event.getDamager() instanceof Player && event.getEntity() instanceof Player){
			
			Player damager = (Player) event.getDamager();
			Player damaged = (Player) event.getEntity();
			
			OPlayer oDamager = pm.getOPlayer(damager);
			OPlayer oDamaged = pm.getOPlayer(damaged);
			
			if(oDamaged != null && oDamager != null){
				if(oDamaged.isInTeamWith(oDamager)){
					event.setCancelled(true);
				}
			}
		}
	}

	private void handleArrow(EntityDamageByEntityEvent event){

		if(!event.isCancelled()){
			PlayersManager pm = PlayersManager.instance();
			
			
			if(event.getEntity() instanceof Player && event.getDamager() instanceof Arrow){
				Projectile arrow = (Projectile) event.getDamager();
				final Player shot = (Player) event.getEntity();
				if(arrow.getShooter() instanceof Player){
					
					final Player shooter = (Player) arrow.getShooter();
					OPlayer oDamager = pm.getOPlayer(shooter);
					OPlayer oDamaged = pm.getOPlayer(shot);

					if(oDamager != null && oDamaged != null){
						if(oDamager.getState().equals(PlayerState.PLAYING) && oDamager.isInTeamWith(oDamaged)){
							event.setCancelled(true);
						}
					}
				}
			}
		}
		
	}
}
